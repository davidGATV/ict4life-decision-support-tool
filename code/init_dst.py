# =============================================================================
# ------------------- DECISION SUPPORT TOOL INITIALIZATION --------------------
# =============================================================================


from decision_support_tool import DecisionSupportTool
# Run dst
def main():
    try:
        # Run DST
        dst = DecisionSupportTool()
        # Run the different modules
        dst.run()
    except Exception as e:
        print(e)
        print('The required input file for the DST was not found.')
if __name__ == '__main__':
    main()

# =============================================================================
# ------------------------------ END ------------------------------------------
# =============================================================================
