# -*- coding: utf-8 -*-
"""
Created on Fri Sep  7 13:50:26 2018

@author: dmarg
"""
import pandas as pd
import numpy as np
from utils.util import get_period
from math import isnan


class Correlation():
    def __init__(self, params):
        self.patientName = params['Name']
        self.patientID = params['ID']
        self.language = params['Lang']
        self.disease = params['Disease']
        self.disease_phase = params['Disease Phase']
        self.comorbidities = params['Comorbidities']
        self.medication = params['Medication']
        self.catalog = params['Catalog']
        self.scales = params['Scales']
        self.probabilities = params['Probabilities']
        self.dates = params['Dates']
        
    def compute_timeSeries_correlation(self):
        print(100 * '-')
        print('5.1) Computing Matrix Correlation ...')
        print(100 * '-')
        period = get_period(self.dates)

        try:
            # Prepare Data
            prediction_list = []
            cols = []
            # Stationary Behaviour
            stationaryBehaviour = self.catalog.get_events(name='stationaryBehaviour')
            if np.var(stationaryBehaviour) > 0:
                prediction_list.append(stationaryBehaviour)
                cols.append('stationaryBehaviour')
            # OverallMotion
            overallMotion = self.catalog.get_events(name='overallMotion')
            if np.var(overallMotion) > 0:
                prediction_list.append(overallMotion)
                cols.append('overallMotion')
            # Visits Bathroom
            visitBathroom = self.catalog.get_events(name='visitBathroom')
            if np.var(visitBathroom) > 0:
                prediction_list.append(visitBathroom)
                cols.append('visitBathroom')
            # Mode Heart rate
            heartRate = self.catalog.get_heartRate_statistics(stat='median')
            if np.var(heartRate) > 0:
                prediction_list.append(heartRate)
                cols.append('heartRate')
            # Mode GSR
            gsr = self.catalog.get_gsr_statistics(stat='median')
            if np.var(gsr) > 0:
                prediction_list.append(gsr)
                cols.append('gsr')
            # Night Motion
            nightMotion = self.catalog.get_events(name='nightMotion')
            if np.var(nightMotion) > 0:
                prediction_list.append(nightMotion)
                cols.append('nightMotion')
            # Abnormal Digital Behaviours
            abnormalDigitalBehaviours = self.catalog.get_events(
                            name='abnormalDigitalBehaviours')
            if np.var(abnormalDigitalBehaviours) > 0:
                prediction_list.append(abnormalDigitalBehaviours)
                cols.append('abnormalDigitalBehaviours')

            # Leaving the House
            leavingHouse = self.catalog.get_events(
                name='leavingHouse')
            if np.var(leavingHouse) > 0:
                prediction_list.append(leavingHouse)
                cols.append('leavingHouse')
            # Daily Motion
            dailyMotion = self.catalog.get_events(
                name='dailyMotion')
            if np.var(dailyMotion) > 0:
                prediction_list.append(dailyMotion)
                cols.append('dailyMotion')

            if self.disease=='Parkinson':
                freezing =  self.catalog.get_events(name='freezing')
                if np.var(freezing) > 0:
                    prediction_list.append(freezing)
                    cols.append('freezing')
                festination = self.catalog.get_events(name='festination')
                if np.var(festination) > 0:
                    prediction_list.append(festination)
                    cols.append('festination')
                lossOfBalance = self.catalog.get_events(name='lossBalance')
                if np.var(lossOfBalance) > 0:
                    prediction_list.append(lossOfBalance)
                    cols.append('lossBalance')
                fallDown = self.catalog.get_events(name='fallDown')
                if np.var(fallDown) > 0:
                    prediction_list.append(fallDown)
                    cols.append('fallDown')
            data = np.array(prediction_list)
            #data = np.concatenate((data, park_data))
            # Round
            data = np.around(data.T, decimals=2)

            # Create a dataframe with the events
            df_symptoms = pd.DataFrame(data)
            df_symptoms.index = period
            df_symptoms.columns = cols

            # Add the medication to the dataframe
            if self.medication is not None:
                for med in self.medication:
                    # Check if the medication is a non-zero vector
                    if np.count_nonzero(med['changes']) > 0:
                        df_symptoms[med['name']] = med['changes']

            # Remove -1 from data
            #df_symptoms[df_symptoms < pd.Timedelta(0)] = np.nan
            df_symptoms.replace(-1, np.nan, inplace=True)
            df_symptoms.dropna(inplace=True, axis=0)
            clean_corr_dict = {}

            # Only when there is data available
            if df_symptoms.shape[0]>0:
                # Compute Correlation
                corr_df = df_symptoms.corr(method='pearson')

                # DataFrame to Dictionary
                corr_dict = corr_df.unstack().to_dict()
                # Remove Nan's
                #clean_corr_dict = filter(lambda k: not isnan(corr_dict[k]), corr_dict)
                clean_corr_dict = {k: corr_dict[k] for k in corr_dict if not isnan(corr_dict[k])}

                # Update Names
                translate = {}
                for k, v in clean_corr_dict.items():
                    translate[k] = '|'.join(k)
                for old, new in translate.items():
                    clean_corr_dict[new] = clean_corr_dict.pop(old)
        except Exception:
            print('Unable to generate the correlation.')
            clean_corr_dict = {}
        return clean_corr_dict
         
    def generate_output(self):
        final_data_corr = self.compute_timeSeries_correlation()
        corr_output = {'Correlations': final_data_corr}
        return corr_output