from utils.util import check_disease_phase
from utils.fuzzy_logic import FuzzyLogic
from utils.statistics_learning import compute_wilcoxon_test

class MotionCognitive:
    def __init__(self, motion_cognitive_params):
        
        """
        patientName, patientID, language, disease,disease_phase, comorbidities,
                 professionals,motion_info,cognitive_info, movement_evolution_info, tipsFile
        """
        # Define main variables and parameters
        self.patientName = motion_cognitive_params['Name']
        self.patientID   = motion_cognitive_params['ID']
        self.language    = motion_cognitive_params['Lang']
        self.disease     = motion_cognitive_params['Disease']
        self.disease_phase = motion_cognitive_params['Disease Phase']
        self.comorbidities = motion_cognitive_params['Comorbidities']
        self.scales = motion_cognitive_params['Scales']
        self.mov_ev = motion_cognitive_params['Movement evolution']
        self.probabilities = motion_cognitive_params['Probabilities']
        self.catalog = motion_cognitive_params['Catalog']
        
        # Scales information
        self.hoehnYard = self.scales[0]['result']
        self.SPMSQ = self.scales[1]
        self.MMSE = self.scales[2]
        self.PDCRS = self.scales[3]
        self.TinettiBergAPM = self.scales[4]
        self.UPDRS = self.scales[5]
        self.APMrigidity = self.scales[6]
        self.PDQ8 = self.scales[7]
        self.lawton = self.scales[8]

    def run(self):
        output_mov = None
        # Run Movement Analysis
        print(100 * '-')
        print('4.1) Analysing Movement for patient ', self.patientID)
        print(100 * '-')
        if self.disease=='Parkinson':
            if self.mov_ev is not None:
                print('Analysing APM scale!')
                output_mov = self.analyse_movementEvolution()
        elif self.disease=='Alzheimer':
            print('Analysing Overall Motion!')
            output_mov = self.analyse_overallMotion()
        
        # Run Cognitive Analysis
        print('Analysing cognitive abilities for patient: ', self.patientID)
        output_cog = self.analyse_cognitiveAbilities()
        
        return output_mov, output_cog

    def analyse_movementEvolution(self):
        
        # Last Evaluation
        self.APM_physio = self.mov_ev[1]['partScores']
        self.total_apm = self.mov_ev[1]['totalScore']
        
        # Movement Analysis
        self.totalAnalysis = self.mov_ev[2]['totalScoreDeviation']
        self.partialAnalysis = self.mov_ev[2]['partsDeviation']

        self.postural = self.APM_physio[0]
        self.balance  = self.APM_physio[1]
        self.walk     = self.APM_physio[2]
        self.invol_movements = self.APM_physio[3]
        self.postural_changes = self.APM_physio[4]
        self.mov_coordination = self.APM_physio[5]
        self.rigidity = self.APM_physio[6]

        output = {'Worry Level':{'postural':-1, 'balance':-1,
                                 'walking':-1, 'inv_movement':-1,
                                 'postural_changes': -1, 'movement_coord':-1,
                                 'rigidity':-1}}
        
        """ The algorithm only considers if the patient gets an score between 0 and 40."""
        if self.total_apm>=0 and self.total_apm<=50:
            # ---------------- 1) Postural
            if self.partialAnalysis[0] <0:
                postural_logic = FuzzyLogic(data_name='Postural', data_value=self.postural,
                                        hoenhyard=self.hoehnYard, totalScore=self.total_apm)
                worry_level = postural_logic.evaluate_movement.output['Worry Level']
                output['Worry Level']['postural'] = worry_level
            # ---------------- 2) Balance
            if self.partialAnalysis[1] < 0:
                balance_logic = FuzzyLogic(data_name='Balance', data_value=self.balance,
                                           hoenhyard=self.hoehnYard, totalScore=self.total_apm)
                worry_level = balance_logic.evaluate_movement.output['Worry Level']
                output['Worry Level']['balance'] = worry_level
            # ---------------- 3) Walk
            if self.partialAnalysis[2] < 0:
                walk_logic = FuzzyLogic(data_name='Walk', data_value=self.walk,
                                           hoenhyard=self.hoehnYard, totalScore=self.total_apm)
                worry_level = walk_logic.evaluate_movement.output['Worry Level']
                output['Worry Level']['walking'] = worry_level
            # ---------------- 4) Involuntary Movements
            if self.partialAnalysis[3] < 0:
                inv_mov_logic = FuzzyLogic(data_name='Involuntary Movements', data_value=self.invol_movements,
                                        hoenhyard=self.hoehnYard, totalScore=self.total_apm)
                worry_level = inv_mov_logic.evaluate_movement.output['Worry Level']
                output['Worry Level']['inv_movement'] = worry_level
            # ---------------- 5) Postural Changes
            if self.partialAnalysis[4] < 0:
                post_changes_logic = FuzzyLogic(data_name='Postural Changes', data_value=self.postural_changes,
                                           hoenhyard=self.hoehnYard, totalScore=self.total_apm)
                worry_level = post_changes_logic.evaluate_movement.output['Worry Level']
                output['Worry Level']['postural_changes'] = worry_level
            # ---------------- 6) Movement Coordination
            if self.partialAnalysis[5] < 0:
                mov_coor_logic = FuzzyLogic(data_name='Movement Coordination', data_value=self.mov_coordination,
                                                hoenhyard=self.hoehnYard, totalScore=self.total_apm)
                worry_level = mov_coor_logic.evaluate_movement.output['Worry Level']
                output['Worry Level']['movement_coord'] = worry_level

            # ---------------- 7) Rigidity
            if self.partialAnalysis[6] < 0:
                rigidity_logic = FuzzyLogic(data_name='Rigidity', data_value=self.rigidity,
                                            hoenhyard=self.hoehnYard, totalScore=self.total_apm)
                worry_level = rigidity_logic.evaluate_movement.output['Worry Level']
                output['Worry Level']['rigidity'] = worry_level
        return output
    
    def analyse_overallMotion(self, alpha=0.05):
        events = self.catalog.get_events(name='overallMotion')
        sd_events = self.catalog.get_events_deviation(name='overallMotion')
        output = compute_wilcoxon_test(events, sd_events, alpha = alpha)
        return output

    def analyse_cognitiveAbilities(self):

        """ Analyse cognitive scales for both parkinson's and Alzheimer's patients"""

        # Mini Mental State Examination (MMSE)
        questions_mmse = self.MMSE['parts']
        questions_spmq = self.SPMSQ['parts']
        output = {'ShortMemory':{'Priority':None,
                                'Recommendation':False},
                  'Calculation':{'Priority':None,
                                'Recommendation': False},
                  'Recall': {'Priority': None,
                             'Recommendation': False}}
        # 1) Temporal Orientation
        
        # 2) Spatial Orientation
        
        # 3) Short Memory
        if (check_disease_phase(self.disease, self.disease_phase) == 1 or
            check_disease_phase(self.disease, self.disease_phase) == 2 and
            questions_mmse[2]<2):
            
            output['ShortMemory']['Priority'] = 'LOW'
            output['ShortMemory']['Recommendation'] = True 
            
        # 4) Calculation
        if check_disease_phase(self.disease, self.disease_phase) == 1 or \
                check_disease_phase(self.disease, self.disease_phase) == 2 and\
                questions_mmse[3]<3:

            output['Calculation']['Priority'] = 'LOW'
            output['Calculation']['Recommendation'] = True 
            
        # 5) Recall
        if check_disease_phase(self.disease, self.disease_phase) == 1 or\
                check_disease_phase(self.disease, self.disease_phase) == 2 and\
                questions_mmse[3]<2:
            output['Recall']['Priority'] = 'LOW'
            output['Recall']['Recommendation'] = True 
           
        # 6) Language

        return output

    def generate_output(self):
        # Run Module
        output_mov, output_cog = self.run()
        motion_cog_output = {'Motion':output_mov,
                             'Cognitive':output_cog}
        return motion_cog_output

    
