# -*- coding: utf-8 -*-
"""
Created on Fri Sep  7 13:52:13 2018

@author: dmarg
"""
import pandas as pd
import numpy as np
from fbprophet import Prophet
from utils.util import get_period,replaceMissingEvents, detect_missing_data

class Prediction():
    def __init__(self, params):
        self.patientName = params['Name']
        self.patientID = params['ID']
        self.language = params['Lang']
        self.disease = params['Disease']
        self.disease_phase = params['Disease Phase']
        self.comorbidities = params['Comorbidities']
        self.medication = params['Medication']
        self.catalog = params['Catalog']
        self.scales = params['Scales']
        self.probabilities = params['Probabilities']
        self.mov_ev = params['Movement evolution']
        self.dates = params['Dates']
    
    def compute_movement_predictions(self, period=7):
        print(100 * '-')
        print('6.1) Executing Time Series Analysis ...')
        print(100 * '-')
        scores = []
        dates = []
        name = 'Movement prediction'
        final_pred = {name:{}}
        if self.mov_ev is not None:
            for element in self.mov_ev:
                for k,v in element.items():
                    if k=='totalScore':
                        scores.append(v)
                    elif k=='date':
                        dates.append(v)
                    else:pass
                        
            mov_data = {'ds':dates, 'y':scores}
            try:
                df = pd.DataFrame(mov_data)
                m = Prophet(n_changepoints=1,weekly_seasonality=True,
                            daily_seasonality=True, changepoint_prior_scale=0.7)
                m.fit(df)

                future = m.make_future_dataframe(periods=period)
                forecast_mov = m.predict(future)

                forecast_mov[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail()
                mov_pred = forecast_mov[['ds', 'yhat', 'yhat_lower', 'yhat_upper']]
                final_pred[name] = mov_pred.to_dict('index')
            except Exception:
                print('Predictions returned NaNs values.')
        return final_pred
    def compute_prediction(self):
        """ Returns a nested dictionary with the corresponding predictions
            for each disease. """
        
        dates = get_period(self.dates)
        zero_threshold = 2

        # Predict Visits bathroom
        visitsBathroom = self.catalog.get_events(name='visitBathroom')
        visitsBathroom = replaceMissingEvents(visitsBathroom)
        visitsBathroom, dates_cleaned = detect_missing_data(event=visitsBathroom, dates=dates, threshold=zero_threshold)
        vbPred = self.analyse_events(event=visitsBathroom, dates=dates_cleaned, eventName='visitBathroom')

        # Predict OverallMotion
        overallMotion = self.catalog.get_events(name='overallMotion')
        overallMotion = replaceMissingEvents(overallMotion)
        overallMotion, dates_cleaned = detect_missing_data(event=overallMotion, dates=dates, threshold=zero_threshold)
        omPred = self.analyse_events(event=overallMotion, dates=dates_cleaned, eventName='overallMotion')

        # Mode Heart rate
        heartRate = self.catalog.get_heartRate_statistics(stat='median')
        heartRate = replaceMissingEvents(heartRate)
        heartRate, dates_cleaned = detect_missing_data(event=heartRate, dates=dates, threshold=zero_threshold)
        heartRatePred = self.analyse_events(heartRate, dates_cleaned,'heartRate')

        # Mode GSR
        gsr = self.catalog.get_gsr_statistics(stat='median')
        gsr = replaceMissingEvents(gsr)
        gsr, dates_cleaned = detect_missing_data(event=gsr, dates=dates, threshold=zero_threshold)
        gsrPred = self.analyse_events(gsr, dates_cleaned, 'gsr')

        # Night Motion
        nightMotion = self.catalog.get_events(name='nightMotion')
        nightMotion = replaceMissingEvents(nightMotion)
        nightMotion, dates_cleaned = detect_missing_data(event=nightMotion, dates=dates,
                                                         threshold=zero_threshold)
        nightMotionPred = self.analyse_events(nightMotion,dates_cleaned,
                                              'nightMotion')
        predictions = [omPred, heartRatePred,
                       gsrPred,nightMotionPred,
                       vbPred]
         
        # Prediction for Parkinson
        if self.disease=='Parkinson':
            # FallDown
            fallDown = self.catalog.get_events(name='fallDown')
            fallDown = replaceMissingEvents(fallDown)
            fallDown, dates_cleaned = detect_missing_data(event=fallDown, dates=dates,
                                                             threshold=zero_threshold)
            fallPred = self.analyse_events(fallDown, dates_cleaned,
                                           'fallDown')
            predictions.append(fallPred)

            # Loss of Balance
            lossBalance = self.catalog.get_events(name='lossBalance')
            lossBalance = replaceMissingEvents(lossBalance)
            lossBalance, dates_cleaned = detect_missing_data(event=lossBalance, dates=dates,
                                                             threshold=zero_threshold)
            lossBalancePred = self.analyse_events(lossBalance, dates_cleaned,
                                                  'lossBalance')
            predictions.append(lossBalancePred)
            
            # Freezing
            freezing = self.catalog.get_events(name='freezing')
            freezing = replaceMissingEvents(freezing)
            freezing, dates_cleaned = detect_missing_data(event=freezing, dates=dates,
                                                             threshold=zero_threshold)
            freezingPred = self.analyse_events(freezing, dates_cleaned, 'freezing')
            predictions.append(freezingPred)
            
            # Festination
            festination = self.catalog.get_events(name='festination')
            festination = replaceMissingEvents(festination)
            festination, dates_cleaned = detect_missing_data(event=festination, dates=dates,
                                                          threshold=zero_threshold)
            festinationPred = self.analyse_events(festination, dates_cleaned,
                                                  'festination')
            predictions.append(festinationPred)
            
            # Movement Evolution
            movPred = self.compute_movement_predictions()
            predictions.append(movPred)

        elif self.disease=='Alzheimer':
            # Abnormal Digital Behaviours
            abnormalDigitalBehaviours = self.catalog.get_events( name='abnormalDigitalBehaviours')
            abnormalDigitalBehaviours = replaceMissingEvents(abnormalDigitalBehaviours)
            abnormalDigitalBehaviours, dates_cleaned = detect_missing_data(event=abnormalDigitalBehaviours,
                                                                           dates=dates,
                                                                           threshold=zero_threshold)
            abnormalDigitalBehavioursPred = self.analyse_events(abnormalDigitalBehaviours,
                                                                dates_cleaned,
                                                                'abnormalDigitalBehaviours')
            predictions.append(abnormalDigitalBehavioursPred)

        return predictions
    
    def analyse_events(self, event, dates, eventName, periods=30, freq='d'):
        name = eventName + '_prediction'
        final_pred = {name:{}}
        try:
            data = {'ds':dates, 'y':event}
            df = pd.DataFrame(data)

            #df[df < pd.Timedelta(0)] = np.nan
            df.replace(-1, np.nan, inplace=True)
            df.dropna(inplace=True, axis=0)

            m = Prophet(weekly_seasonality=True, daily_seasonality=True, changepoint_prior_scale=0.7)
            m.fit(df)

            future = m.make_future_dataframe(periods=periods, freq=freq, include_history=False)
            forecast = m.predict(future)
            forecast_data = forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']]

            fdates = [i.strftime('%Y-%m-%d') for i in list(forecast_data['ds'])]
            yhat = list(forecast_data['yhat'])
            yhat_lower = list(forecast_data['yhat_lower'])
            yhat_upper = list(forecast_data['yhat_upper'])

            model_data = {'Dates':fdates, 'Prediction':yhat, 'lower_ci':yhat_lower, 'upper_ci':yhat_upper}
            #final_pred = {name:forecast_data.to_dict('index')}
            final_pred = {name:model_data}
        except Exception:
            print('Predictions returned NaNs values.')
        return final_pred
    
    def generate_output(self):
        final_data = self.compute_prediction()
        prediction_output = {'Predictions': final_data}
        return prediction_output