# ======================================================================================================================
# ------------------------------------------------ SYMPTOMS MODULE -----------------------------------------------------
# ======================================================================================================================

from utils.statistics_learning import (maximum_a_posteriori_decisor)

class Symptoms():
    def __init__(self, symptoms_params):
        self.patientName = symptoms_params['Name']
        self.patientID = symptoms_params['ID']
        self.language = symptoms_params['Lang']
        self.disease = symptoms_params['Disease']
        self.disease_phase = symptoms_params['Disease Phase']
        self.comorbidities = symptoms_params['Comorbidities']
        self.professionals_associated = symptoms_params['Professionals'][1]
        self.medication = symptoms_params['Medication']
        self.recommendation_file = symptoms_params['TipsFile']
        self.general_symptoms = symptoms_params['General symptoms']
        self.disease_symptoms = symptoms_params['Disease symptoms']
        self.date = symptoms_params['Date']
        self.catalog = symptoms_params['Catalog']
        self.alzheimer_dict = {}
        self.parkinson_dict = {}

        

    # ==================================================================================================================
    def run(self):
        # Analyse symptoms
        alzheimer_sympt, parkinson_sympt, general_sympt = self.analyse_symptoms()
        output = [alzheimer_sympt, parkinson_sympt, general_sympt]
        return output
    # ==================================================================================================================
    def analyse_symptoms(self):
        alzheimer_dict = {'analysis':{}}
        parkinson_dict = {'analysis':{}}
        general_symptoms = {'analysis':{}}
        # General Symptoms
        print(100*'-')
        print('2.1) Analyzing General Symptoms for patient ', self.patientID)
        print(100 * '-')
        general_symptoms['analysis'] = self.analyse_general_symptoms()

        # Alzheimer's case
        if self.disease=='Alzheimer':
            print(100 * '-')
            print('2.2) Analyzing Alzheimer symptoms for patient ', self.patientID)
            print(100 * '-')
            alzheimer_dict['analysis'] = self.analyse_alzheimer_symptoms()
        # Parkinson's case
        elif self.disease=='Parkinson':
            print(100 * '-')
            print('2.2) Analyzing Parkinson symptoms for patient ', self.patientID)
            print(100 * '-')
            parkinson_dict['analysis'] = self.analyse_parkinson_symptoms()
        else:
            pass
        return alzheimer_dict, parkinson_dict,general_symptoms
    
    # ==================================================================================================================
    def analyse_general_symptoms(self):

        # 1) Apathy Model
        p_apathy = self.general_symptoms['Apathy'][0]
        p_apathy_dailyMotion = self.general_symptoms['Apathy'][1]
        p_apathy_digitalTimeUsage = self.general_symptoms['Apathy'][2]
        p_apathy_leavingHouse = self.general_symptoms['Apathy'][3]
        p_apathy_overalMotion = self.general_symptoms['Apathy'][4]
        p_apathy_stationaryBehaviour =self.general_symptoms['Apathy'][5]

        # 1.1) Analyse Apathy Model
        apathy_analysis = self.apathy_model_analysis(p_apathy, p_apathy_dailyMotion,
                                                     p_apathy_digitalTimeUsage,
                                                     p_apathy_leavingHouse,
                                                     p_apathy_overalMotion,
                                                     p_apathy_stationaryBehaviour)
        # 2) Hypertension Model
        p_hipertension = self.general_symptoms['Hypertension'][0]
        p_hiperTen_bioMed = self.general_symptoms['Hypertension'][1]
        p_hiperTen_medCond = self.general_symptoms['Hypertension'][2]

        # 2.1) Analyse Hypertension Model
        hypertension_analysis = self.hypertension_model_analysis(p_hipertension, p_hiperTen_bioMed,
                                                                 p_hiperTen_medCond)


        # 3) Cardiovascular Model
        p_cardiovascularCond = self.general_symptoms['Cardiovascular'][0]
        p_cardiovascularCond_bioMes = self.general_symptoms['Cardiovascular'][1]
        p_cardiovascularCond_medCond = self.general_symptoms['Cardiovascular'][2]

        # 3.1) Analyse Cardiovascular Model
        cardiovascular_analysis = self.cardiovascular_model_analysis(p_cardiovascularCond, p_cardiovascularCond_bioMes,
                                                                   p_cardiovascularCond_medCond)

        # 4) Digital Addiction
        p_digAdd = self.general_symptoms['DigitalAddiction'][0]
        p_digAdd_dep = self.general_symptoms['DigitalAddiction'][1]
        p_digAdd_DTU = self.general_symptoms['DigitalAddiction'][2]

        # 4.1) Analyse Digital Addiction
        digitalAddiction_analysis = self.digitalAddiction_model_analysis(p_digAdd, p_digAdd_dep, p_digAdd_DTU)


        # 5) Digital Confusion
        p_digConf = self.general_symptoms['DigitalConfusion'][0]
        p_digConf_ADB = self.general_symptoms['DigitalConfusion'][1]

        # 5.1) Analyse Digital Confusion
        digitalConfusion_analysis = self.digitalConfusion_model_analysis(p_digConf, p_digConf_ADB)

        # 6) Create Output General Symptoms
        general_symptoms_output = {'Apathy': apathy_analysis, 'Hypertension':hypertension_analysis,
                                   'Cardiovascular': cardiovascular_analysis,
                                   'DigitalAddiction': digitalAddiction_analysis,
                                   'DigitalConfusion':digitalConfusion_analysis}
        return general_symptoms_output

# ======================================================================================================================
    def apathy_model_analysis(self, p_apathy, p_apathy_dailyMotion, p_apathy_digitalTimeUsage,
                              p_apathy_leavingHouse, p_apathy_overalMotion, p_apathy_stationaryBehaviour,
                              threshold=0.6):
        # Trigger the model
        if p_apathy>=threshold:
            # Get Maximum a posteriori
            posterior_data = {'probabilitites':[p_apathy_dailyMotion, p_apathy_digitalTimeUsage,
                                                p_apathy_leavingHouse, p_apathy_overalMotion,
                                                p_apathy_stationaryBehaviour],
                              'causes':['lackPhysicalActivities',
                                        'digitalAddiction',
                                        'lackLeisureActivities',
                                        'lackPhysicalActivities',
                                        'sedentarism']}
            cause, prob = maximum_a_posteriori_decisor(posterior_data)
            apathy_analysis = {'Cause':cause, 'Probability':prob}
        else:
            return None

        return apathy_analysis

    def hypertension_model_analysis(self, p_hipertension, p_hiperTen_bioMed, p_hiperTen_medCond,threshold=0.6):
        # Trigger the model
        if p_hipertension >= threshold:
            # Get Maximum a posteriori
            posterior_data = {'probabilitites': [p_hiperTen_bioMed, p_hiperTen_medCond],
                              'causes': ['biologicalMeasurements',
                                         'medicationChange']}
            cause, prob = maximum_a_posteriori_decisor(posterior_data)
            hypertension_analysis = {'Cause': cause, 'Probability': prob}
        else:
            return None
        return hypertension_analysis

    def cardiovascular_model_analysis(self, p_cardiovascularCond, p_cardiovascularCond_bioMes,
                                      p_cardiovascularCond_medCond, threshold=0.6):
        # Trigger the model
        if p_cardiovascularCond >= threshold:
            # Get Maximum a posteriori
            posterior_data = {'probabilitites': [p_cardiovascularCond_bioMes, p_cardiovascularCond_medCond],
                              'causes': ['biologicalMeasurements',
                                         'medicationChange']}
            cause, prob = maximum_a_posteriori_decisor(posterior_data)
            cardiovascular_analysis = {'Cause': cause, 'Probability': prob}
        else:
            return None
        return cardiovascular_analysis

    def digitalAddiction_model_analysis(self, p_digAdd, p_digAdd_dep, p_digAdd_DTU, threshold=0.6):
        # Trigger the model
        if p_digAdd >= threshold:
            # Get Maximum a posteriori
            posterior_data = {'probabilitites': [p_digAdd_dep, p_digAdd_DTU],
                              'causes': ['depresion',
                                         'digitalAddiction']}
            cause, prob = maximum_a_posteriori_decisor(posterior_data)
            digitalAddiction_analysis = {'Cause': cause, 'Probability': prob}
        else:
            return None
        return digitalAddiction_analysis

    def digitalConfusion_model_analysis(self, p_digConf, p_digConf_ADB, threshold=0.6):
        # Trigger the model
        if p_digConf >= threshold:
            # Get Maximum a posteriori
            posterior_data = {'probabilitites': [p_digConf_ADB],
                              'causes': ['abnormalBehaviour']}
            cause, prob = maximum_a_posteriori_decisor(posterior_data)
            digitalConfusion_analysis = {'Cause': cause, 'Probability': prob}
        else:
            return None
        return digitalConfusion_analysis

    def confusion_model(self, p_confusion, p_confusion_ABE, p_confusion_neuCom,
                        p_confusion_psychiaCom, threshold=0.6):
        # Trigger the model
        if p_confusion >= threshold:
            # Get Maximum a posteriori
            posterior_data = {'probabilitites': [p_confusion_ABE, p_confusion_neuCom,
                                                 p_confusion_psychiaCom],
                              'causes': ['abnormalBehaviours',
                                         'neurologicConditions',
                                         'psychiatricConditions']}
            cause, prob = maximum_a_posteriori_decisor(posterior_data)
            confusion_analysis = {'Cause': cause, 'Probability': prob}
        else:
            return None
        return confusion_analysis

    def parkinson_events_model(self, p_parkEvent, p_parkEvent_fallDown, p_parkEvent_festination,
                               p_parkEvent_freezing, p_parkEvent_lossBalance, p_parkEvent_MedChange,
                               threshold=0.6):
        # Trigger the model
        if p_parkEvent >= threshold:
            # Get Maximum a posteriori
            posterior_data = {'probabilitites': [p_parkEvent_fallDown, p_parkEvent_festination,
                                                 p_parkEvent_freezing, p_parkEvent_lossBalance,
                                                 p_parkEvent_MedChange],
                              'causes': ['falldown',
                                         'festination',
                                         'freezing',
                                         'lossofbalance',
                                         'medicationchange']}
            cause, prob = maximum_a_posteriori_decisor(posterior_data)
            parkinson_analysis = {'Cause': cause, 'Probability': prob}
        else:
            return None
        return parkinson_analysis

# ======================================================================================================================
    def analyse_alzheimer_symptoms(self):
        alzheimer_symptoms_output = {'Events':None}
        # 1) Confusion Model
        p_confusion = self.disease_symptoms['Confusion'][0]
        p_confusion_ABE = self.disease_symptoms['Confusion'][1]
        p_confusion_neuCom = self.disease_symptoms['Confusion'][2]
        p_confusion_psychiaCom = self.disease_symptoms['Confusion'][3]

        # 1.1) Analyse Confusion Model
        confusion_analysis = self.confusion_model(p_confusion, p_confusion_ABE,
                                                  p_confusion_neuCom, p_confusion_psychiaCom)

        alzheimer_symptoms_output['Events'] = confusion_analysis
        return alzheimer_symptoms_output

    def analyse_parkinson_symptoms(self):
        parkinson_symptoms_output = {'Events':None}
        # 1) Parkinson Events Model
        p_parkEvent = self.disease_symptoms['Events'][0]
        p_parkEvent_fallDown = self.disease_symptoms['Events'][1]
        p_parkEvent_festination = self.disease_symptoms['Events'][2]
        p_parkEvent_freezing = self.disease_symptoms['Events'][3]
        p_parkEvent_lossBalance = self.disease_symptoms['Events'][4]
        p_parkEvent_MedChange = self.disease_symptoms['Events'][5]

        # 1.1) Analysis Parkinson Events
        parkinson_events_analysis = self.parkinson_events_model(p_parkEvent, p_parkEvent_fallDown, p_parkEvent_festination,
                                                                p_parkEvent_freezing, p_parkEvent_lossBalance,
                                                                p_parkEvent_MedChange)
        parkinson_symptoms_output['Events'] = parkinson_events_analysis
        return parkinson_symptoms_output

    def generate_output(self):
        # Run the Model
        output =  self.run()
        
        # Generate the output of the model
        symptoms_output = {'Parkinson Symptoms': output[1],
                            'Alzheimer Symptoms': output[0],
                            'General Symptoms': output[2]}
        return symptoms_output
       
