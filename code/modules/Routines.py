
# =============================================================================
# ---------------------- ROUTINES MODULE --------------------------------------
# =============================================================================

import numpy as np
from utils.util import (check_disease_phase,parse_probabilities)
from utils.statistics_learning import (compute_evidence_probability,
                                       compute_wilcoxon_test)

class Routines():
    def __init__(self, routines_params):
        """ 
        """
        # Define main variables for routines
        self.patientName = routines_params['Name']
        self.patientID = routines_params['ID']
        self.language = routines_params['Lang']
        self.disease = routines_params['Disease']
        self.disease_phase = routines_params['Disease Phase']
        self.comorbidities = routines_params['Comorbidities']
        self.professionals_associated = routines_params['Professionals'][1]
        self.medication = routines_params['Medication']
        self.recommendation_file = routines_params['TipsFile']
        self.probabilities = routines_params['Probabilities']
        self.insomnia = routines_params['Insomnia']
        self.night_motion = routines_params['NightMotion']
        self.scales = routines_params['Scales']
        self.visits_bathroom = routines_params['VisitsBathroom']
        self.incontinence = routines_params['Incontinence']
        self.catalog = routines_params['Catalog']
        self.routine_message = {}
        

    # ==================================================================================================================
    # ============================================= MAIN FUNCTION ======================================================
    # ==================================================================================================================

    def run(self):
        """ This module evaluates different symptoms which are relevant for
            Sleeping disorders and the number of visits to the bathroom and makes
            recommendations about how to act on those situations.
       """
        self.sleeping_disorders_dict = {}
        self.visits_bathroom_dict = {}

        # ---------------------------------------------------------------------
        #                                   1. Sleeping Disorders
        # ---------------------------------------------------------------------

        self.p_insomnia_nightMotion = parse_probabilities(self.probabilities, 'Insomnia|nightMotion')
        self.p_insomnia_digitalAddict = parse_probabilities(self.probabilities, 'Insomnia|digitalAddiction')
        self.p_insomnia_depression = parse_probabilities(self.probabilities, 'Insomnia|depression')
        self.p_insomnia_medicalCond = parse_probabilities(self.probabilities, 'Insomnia|medicalCondition')
        self.p_sleeping_disorder =  parse_probabilities(self.probabilities, 'sleepDisorders')
        
        self.p_digitalAddiction_DTS = parse_probabilities(self.probabilities, 'digitalAddiction|digitalTimeUsage')
        self.p_digitalAddiction = parse_probabilities(self.probabilities, 'digitalAddiction')
        self.p_digitalAddiction_dep = parse_probabilities(self.probabilities, 'digitalAddiction|depression')
        
        # Initial Value
        self.prob_medication_change = 0

        self.GDS_SF = self.scales[8] #self.sleeping_disorder_info[10]

        # Analyse sleeping disorders
        print(100*'-')
        print('1.1) Analysing Sleeping disorders')
        print(100 * '-')
        sleeping_disorder_output= self.analyse_sleeping_disorder()

        # --------------------------------------------------------------------------------------------------------------
        #                                  2. Visits to the bathroom
        # --------------------------------------------------------------------------------------------------------------
        
        self.p_incont_visitsBath = parse_probabilities(self.probabilities, name='Incontinence|visitsBathroom')
        self.p_incont_medicalCond = parse_probabilities(self.probabilities, name='Incontinence|nedicalCondition')
        self.p_incontinence = parse_probabilities(self.probabilities, name='Incontinence')

        # Analyse visits to the bathroom
        print(100 * '-')
        print('1.2) Visits to the Bathroom ')
        print(100 * '-')
        visits_bathroom_output = self.analyse_visits_bathroom()
        # Feed the routine dictionary with all the gathered information
        routine_output = {'Sleeping Disorders': sleeping_disorder_output,
                          'Visits Bathroom': visits_bathroom_output}
        
        return routine_output
    
    # ==================================================================================================================
    # ==================================================================================================================
    # ==================================================================================================================
    def maximum_likelihood_NM(self):
        """ This method computes the likelihood probabilities for several symptoms
            and returns the maximum value of the likelihood and its cause.
        """
    
        print("ML P(insomnia | NM): ", self.p_insomnia_nightMotion)
        print("ML P(insomnia | Dep): ", self.p_insomnia_depression)
        print("ML P(insomnia | Digital Addiction): ", self.p_insomnia_digitalAddict)
        print("ML P(insomnia | Medical Condition ): ", self.p_insomnia_medicalCond)
    
        # Compute the maximum Likelihood
        ml_solution = np.array([self.p_insomnia_nightMotion,
                                self.p_insomnia_depression,
                                self.p_insomnia_digitalAddict,
                                self.p_insomnia_medicalCond])
    
        # Get the index and the maximum value
        index = np.argmax(ml_solution)
        max_prob = ml_solution[index]
        
        if index==0:
            cause = 'insomnia'
        elif index==1:
            cause = 'depression'
        elif index==2:
            cause = 'digital addiction'
        else:
            cause = 'medication change'
        
        return cause, max_prob
    
    def analyse_sleeping_disorder(self, threshold=0.6):
        """ Analyses the Sleeping disorder of a given patient considering insomnia, depression, digital addiction and
            Medical changes as the main causes.
        """
        sleeping_dis_output = {'Recommendation':False,
                               'Cause':None,
                               'Priority':None}
        
        sd_events  = self.catalog.get_events_deviation(name='nightMotion')
        events = self.catalog.get_events(name='nightMotion') 
        
        # 1) Compute evidence probability of night Motion
        p_nightMotion = compute_evidence_probability(events)
        
        # 2) Compute Wilcoxon test
        test_output = compute_wilcoxon_test(events, sd_events)
        
        if (p_nightMotion>=threshold and test_output['Reject']):
            # Increase in nightMotion
            if test_output['Change']=='increase':
                # Find most probable cause
                cause, max_prob = self.maximum_likelihood_NM()
                # Generate Output
                sleeping_dis_output['Recommendation'] = True
                sleeping_dis_output['Cause'] = cause
                sleeping_dis_output['Priority'] = 'MEDIUM'

        return sleeping_dis_output
    
    def analyse_visits_bathroom(self, threshold=0.6):
        visits_bathroom_output = {'Recommendation':False,
                                  'Cause':None,
                                  'Priority':None,
                                  'Change':None}
        sd_events  = self.catalog.get_events_deviation(name='visitBathroom')
        events = self.catalog.get_events(name='visitBathroom') 
        
        disease_phase = check_disease_phase(self.disease, self.disease_phase)
        p_incontinence_medical_cond = parse_probabilities(self.probabilities, 
                                                          'Incontinence|nedicalCondition')
        # 1) Compute evidence probability of night Motion
        p_visitsBathroom = compute_evidence_probability(events)
        p_incontinence = parse_probabilities(self.probabilities,
                                            'Incontinence')
        # 2) Compute Wilcoxon test
        test_output = compute_wilcoxon_test(events, sd_events)
        
        # High probability and significant change
        if (p_visitsBathroom>=threshold and test_output['Reject']):
            
            # Increase
            if test_output['Change']=='increase':
                # Find most probable cause
                if self.incontinence==1 or p_incontinence>=threshold:
                    # Check disease phase
                    if disease_phase==2:
                        priority = 'MEDIUM'
                    elif disease_phase ==1:
                        priority = 'HIGH'
                    else:
                        priority = 'LOW'
                    visits_bathroom_output['Priority'] = priority
                    visits_bathroom_output['Recommendation']=True
                    visits_bathroom_output['Cause']= 'incontinence'
                    visits_bathroom_output['Change'] = test_output['Change']
                    
                elif (self.incontinence==0 and p_incontinence_medical_cond>=threshold):
                    if disease_phase==2:
                        priority = 'MEDIUM'
                    elif disease_phase ==1:
                        priority = 'HIGH'
                    else:
                        priority = 'LOW'
                    visits_bathroom_output['Priority'] = priority
                    visits_bathroom_output['Recommendation']=True
                    visits_bathroom_output['Cause']= 'medication change'
                    visits_bathroom_output['Change'] = test_output['Change']
            # Decrease
            elif test_output['Change']=='decrease':
                if self.incontinence==1 or p_incontinence>=threshold:
                    if disease_phase==2:
                        priority = 'MEDIUM'
                    elif disease_phase ==1:
                        priority = 'HIGH'
                    else:
                        priority = 'LOW'
                    visits_bathroom_output['Priority'] = priority
                    visits_bathroom_output['Recommendation']=True
                    visits_bathroom_output['Cause']= 'lack of visits to the bathroom'
                    visits_bathroom_output['Change'] = test_output['Change']
                elif (self.incontinence==0 and p_incontinence_medical_cond>=threshold):
                    if disease_phase==2:
                        priority = 'MEDIUM'
                    elif disease_phase ==1:
                        priority = 'HIGH'
                    else:
                        priority = 'LOW'
                    visits_bathroom_output['Priority'] = priority
                    visits_bathroom_output['Recommendation']=True
                    visits_bathroom_output['Cause']= 'medication change'
                    visits_bathroom_output['Change'] = test_output['Change']
            else:
                pass
        
        return visits_bathroom_output
    
    # =========================================================================
    def generate_output(self):
        routines_output = self.run() 
        return routines_output