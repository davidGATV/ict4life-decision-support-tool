from utils.util import check_disease_phase
from communication.communication import Communication
from communication.domain.messageValueObject import MessageVO
from translate import Translator

# =============================================================================
# ------------------------------- NOTIFICATION --------------------------------
# =============================================================================

class Notification:
    def __init__(self, patient_information, professionals, data,
                 tipsFile, language):
        self.patient_information = patient_information
        self.patient_Name = self.patient_information['Patient Name']
        self.patientID = self.patient_information['Patient ID']
        self.disease = self.patient_information['Disease']
        self.disease_phase = self.patient_information['Disease Phase']
        self.lang = language
        self.from_lang = 'EN'
        self.professionals = professionals[1]
        self.data = data
        self.tipsFile = tipsFile
        self.notification_schedule = {'Professionals': {'psychologistID': False,
                                                        'psychiatristID': False,
                                                        'neurologistID': False,
                                                        'physiotherapistID': False,
                                                        'caregiverID': False,
                                                        'socialWorkerID': False,
                                                        'dailyCenterWorkerID': False},
                                      'Patient': self.send_notification_patient()}
        # Notification basis
        self.priority=None
        self.notification_title = None
        self.notification_content = None
        
    def set_up_notification_message(self, informative_data = None,
                                    cause=None, tip=None):
        self.cause = cause
        self.tip= tip
        self.informative_data = informative_data
        
        notification = ''
        
        # Check for informative data
        if self.informative_data is not None:
            notification += self.informative_data
        
        # Check for cause data
        if self.cause is not None:
            notification += self.cause
        
        # Check for tip
        if self.tip is not None:
            notification += self.tip
        
        # Add the name of the patient in case it fits.
        notification = notification.replace('+++', self.patient_Name)
        
        if self.lang is not 'EN':
            # Translate
            notification = self.translate_notification(notification)
            notification.replace('+++', self.patient_Name)
        return notification
    
    def translate_notification(self, notification):
        translator= Translator(from_lang=self.from_lang, 
                               to_lang=self.lang)
        notification_trans = translator.translate(notification)
        return notification_trans
    
    def send_notification(self, priority=None, title=None, content=None,
                          module_name=None,uuid_to=None):
        if priority is None:
            priority=self.get_priority(module_name)

        if (priority is not None and title is not None and content is not None and uuid_to is not None):

            try:
                dst_notification = MessageVO(title=title, text=content,uuid_to=uuid_to, priority=priority)
                com = Communication(dst_notification)
                com.send
            except Exception as e:
                raise ValueError('An error occurred during the sending procedure.')
        else:
            return False
        return True
    
    def send_notification_patient(self):
        send_notification = False
        if (check_disease_phase(self.disease, self.disease_phase)==1 or
            check_disease_phase(self.disease, self.disease_phase)==2):
            send_notification = True
        return send_notification
    
    def notification_by_diseasePhase(self):
        notification_type = -1
        if check_disease_phase(self.disease, self.disease_phase)==1:
            notification_type = 1
        elif check_disease_phase(self.disease, self.disease_phase)==2:
            notification_type = 2
        else:
            pass
        return notification_type
    
    def get_priority(self, module_name='Routines'):
        priority = None
        if (module_name=='Routines' or module_name=='Training'):
            if (self.notification_by_diseasePhase()==1 or 
                self.notification_by_diseasePhase()==2):
                priority = 'MEDIUM'
            else:
                priority = 'LOW'
        elif (module_name=='Symptoms' or module_name=='MotionCognitive'):
            if self.notification_by_diseasePhase()==1:
                priority = 'HIGH'
            elif self.notification_by_diseasePhase()==2:
                priority = 'MEDIUM'
            else:
                priority = 'LOW'
        else:
            pass
        return priority
    
    def get_notification_tittle(self):
        return
    
    def get_tip_key(self, profID=None, patientID=None):
        key_pat = None
        key_prof = None
        if self.notification_by_diseasePhase()==1:
            # Send to patient of disease phase 1 and professionals
            if patientID is not None and self.send_notification_patient():
                key_pat = 'pat1'
                
            if profID is not None:        
                # Send to caregivers 
                if profID =='caregiverID':
                    key_prof = 'car1'
                elif profID == 'socialWorkerID' or profID == 'dailyCenterWorkerID':
                    key_prof = 'wor1'
                elif (profID == 'psychologistID' or profID == 'psychiatristID' or
                      profID == 'neurologistID'):
                    key_prof = 'prof1'
                else:
                    key_prof = 'phi1'
                        
        elif self.notification_by_diseasePhase()==2:
            # Send to patients of disease phase 1 and professionals
            if patientID is not None and self.send_notification_patient():
                key_pat = 'pat2'
            # Send to caregivers
            if profID is not None:      
                if profID =='caregiverID':
                    key_prof = 'car2'
                elif profID == 'socialWorkerID' or profID == 'dailyCenterWorkerID':
                    key_prof = 'wor2'
                elif (profID == 'psychologistID' or profID == 'psychiatristID' or
                      profID == 'neurologistID'):
                    key_prof = 'prof2'
                else:
                    key_prof = 'phi2'               
        else:
            key_pat = None
            if profID is not None:      
                if profID =='caregiverID':
                    key_prof = 'car2'
                elif profID == 'socialWorkerID' or profID == 'dailyCenterWorkerID':
                    key_prof = 'wor2'
                elif (profID == 'psychologistID' or profID == 'psychiatristID' or
                      profID == 'neurologistID'):
                    key_prof = 'prof2'
                else:
                    key_prof = 'phi2'
        
        return (key_pat, key_prof)
    
    def obtain_personal_notification_from_file(self, data_dict, info, key_tip, cause):
        notification_content_prof = {}
        notification_content_pat = {'patient':None}
        
        for profID, notif in self.notification_schedule['Professionals'].items():
            if notif:
                key_pat, key_prof = self.get_tip_key(profID)        
                tip_prof = data_dict[key_tip][key_prof]
                notification_content_prof[profID] = self.set_up_notification_message(informative_data=info,
                                                                                     cause=cause,
                                                                                     tip=tip_prof)
        # Generate notification for patient
        key_pat,key_prof = self.get_tip_key(patientID=self.patientID)
        tip_pat = None
        if key_pat is not None:
            tip_pat = data_dict[key_tip][key_pat]
        notification_content_pat['patient'] = self.set_up_notification_message(informative_data=None,
                                                                               cause=None,
                                                                               tip=tip_pat)
        return [notification_content_prof, notification_content_pat]
        
# =============================================================================

class RoutinesNotification(Notification):
    def __init__(self, patient_information, professionals, data,
                 language, tipsFile):
        super().__init__(patient_information, professionals,
              data, tipsFile, language)
        self.notification_schedule = {'Professionals': {'psychologistID':False,
                                                        'psychiatristID':False,
                                                        'neurologistID':False,
                                                        'physiotherapistID':False,
                                                        'caregiverID':True,
                                                        'socialWorkerID':True,
                                                        'dailyCenterWorkerID':True},
                                      'Patient':self.send_notification_patient()}
        
    def generate_tips(self):
        routines_tips = {'Sleeping Disorder': self.sleepingDisorder_tips(),
                         'Visits Bathroom': self.visitsBathroom_tips()}
        return routines_tips
    
    def sleepingDisorder_tips(self):
        sleep_dis_data = self.data['Sleeping Disorders']
        notifications = None
        cause = None
        key_tip = None
        # Check for recommendation
        if (sleep_dis_data['Recommendation'] and 
            sleep_dis_data['Cause'] is not None):
            sd_dict = self.tipsFile[self.from_lang]['routines']['sleepingDisorders']
            info = sd_dict['detection']
            # Generate Tip depending on the cause
            if sleep_dis_data['Cause']== 'insomnia':
                cause = sd_dict['insomnia']
                key_tip = 'insomniaTips'
            elif sleep_dis_data['Cause']== 'depression':
                cause = sd_dict['depression']
                key_tip = 'depressionTips'
            elif sleep_dis_data['Cause']== 'digital addiction':
                cause = sd_dict['digital addiction'] 
                key_tip = 'digitalAddictionTips'
            elif sleep_dis_data['Cause']== 'medication change':
                cause = sd_dict['medication change'] 
                key_tip = 'medicationChangeTips'
            else:
                pass
            notifications = self.obtain_personal_notification_from_file(data_dict=sd_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
        return notifications
    
    def visitsBathroom_tips(self):
        visitsBath_data = self.data['Visits Bathroom']
        notifications = None
        cause = None
        key_tip = None

        # Check for recommendation
        if (visitsBath_data['Recommendation'] and
            visitsBath_data['Cause'] is not None):
            
            vb_dict = self.tipsFile[self.from_lang]['routines']['visitsBathroom']
            change = visitsBath_data['Change']
            info = vb_dict[change]
            
            # Generate Tip depending on the cause
            if visitsBath_data['Cause']== 'incontinence':
                cause = vb_dict['incontinence']
                key_tip = 'incontinenceTips'
            elif visitsBath_data['Cause']== 'medication change':
                cause = vb_dict['medication change']
                key_tip = 'medicationChangeTips'
            elif visitsBath_data['Cause']== 'lack of visits to the bathroom':
                cause = vb_dict['lossOfMind']
                key_tip = 'lackVisitsTips'
            else:
                pass
            notifications = self.obtain_personal_notification_from_file(data_dict=vb_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
        return notifications

    def launch_notification(self):
        routines_notifications = self.generate_tips()
        try:
            data = routines_notifications['Sleeping Disorder']  
            # 1) Sleeping Disorder
            if data is not None and len(data)>0:
                # List of tuples
                for index, notif in enumerate(data):
                    if index==0:
                        # Professional Notification
                        # Tuple of dictionaries
                        for profID, message in notif.items():
                            # create notification
                            for uuid_prof in self.professionals[profID]:
                                uuid_to = uuid_prof
                                content = message
                                priority = self.get_priority(module_name='Routines')
                                self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                       title='ICT4LIFE Sleeping Disorder Recommendation')
                    else:
                        # Patient Notification
                        for k, message in notif.items():
                            uuid_to = self.patientID
                            content = message
                            priority = self.get_priority(module_name='Routines')
                            self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                   title='ICT4LIFE Sleeping Recommendation')
            # 2) Visits Bathroom
            data = routines_notifications['Visits Bathroom']  
            if data is not None and len(data)>0:
                # List of tuples
                for index, notif in enumerate(routines_notifications['Visits Bathroom']):
                    # Professional Notification
                    # Tuple of dictionaries
                    if index==0:
                        for profID, message in notif.items():
                            # create notification
                            for uuid_prof in self.professionals[profID]:
                                uuid_to = uuid_prof
                                content = message
                                priority = self.get_priority(module_name='Routines')
                                self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                       title='ICT4LIFE visits to the bathroom report')
                    else:
                        # Patient Notification
                        for k, message in notif.items():
                            uuid_to = self.patientID
                            content = message
                            priority = self.get_priority(module_name='Routines')
                            self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                   title='ICT4LIFE Recommendation')
        except Exception as e:
            print(e)
            raise ValueError('An error occurred during the sending process of routines recommendations')
        return
# =============================================================================
# =============================================================================
class SymptomsNotification(Notification):
    def __init__(self, patient_information, professionals, data,
                 language, tipsFile):
        super().__init__(patient_information, professionals,
              data,tipsFile, language)
        self.notification_schedule = {'Professionals': {'psychologistID':False,
                                                        'psychiatristID':False,
                                                        'neurologistID':True,
                                                        'physiotherapistID':False,
                                                        'caregiverID':True,
                                                        'socialWorkerID':True,
                                                        'dailyCenterWorkerID':True},
                                      'Patient':self.send_notification_patient()}
        
    def generate_tips(self):
        symptoms_tips = {'General Symptoms': self.general_symptoms_tips(),
                         'Parkinson Symptoms': None,
                         'Alzheimer Symptoms': None}
        # Analyse depending on the disease
        if self.disease == 'Parkinson':
            symptoms_tips['Parkinson Symptoms'] = self.parkinson_symptoms_tips()
        elif self.disease == 'Alzheimer':
            symptoms_tips['Alzheimer Symptoms'] = self.alzheimer_symptoms_tips()
        else:
            pass
        return symptoms_tips
        
    def general_symptoms_tips(self):
        generalSymp_data = self.data['General Symptoms']['analysis']
        gs_dict = self.tipsFile[self.from_lang]['symptoms']['generalSymptoms']
        notifications = None

        # Apathy Notification
        if generalSymp_data['Apathy'] is not None:
            info = gs_dict['apathyDetection']
            cause = gs_dict[generalSymp_data['Apathy']['Cause']]
            key_tip = 'apathyTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=gs_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
        # Hypertension
        if generalSymp_data['Hypertension']is not None:
            info = gs_dict['hypertensionDetection']
            cause = gs_dict[generalSymp_data['Hypertension']['Cause']]
            key_tip = 'hypertensionTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=gs_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
        # Cardiovascular
        if generalSymp_data['Cardiovascular'] is not None:
            info = gs_dict['cardiovascularDetection']
            cause = gs_dict[generalSymp_data['Cardiovascular']['Cause']]
            key_tip = 'cardiovascularTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=gs_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
        # Digital Addiction
        if generalSymp_data['DigitalAddiction'] is not None:
            info = gs_dict['digitalAddicDetection']
            cause = gs_dict[generalSymp_data['DigitalAddiction']['Cause']]
            key_tip = 'digitalAddictionTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=gs_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
        # Digital Confusion
        if generalSymp_data['DigitalConfusion'] is not None:
            info = gs_dict['digitalConfDetection']
            cause = gs_dict[generalSymp_data['DigitalConfusion']['Cause']]
            key_tip = 'digitalConfusionTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=gs_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
        return notifications
    
    def parkinson_symptoms_tips(self):
        parkSymp_data = self.data['Parkinson Symptoms']['analysis']
        ps_dict = self.tipsFile[self.from_lang]['symptoms']['parkinsonSymptoms']
        notifications = None

        if parkSymp_data['Events'] is not None:
            info = ps_dict['detection']
            cause = ps_dict[parkSymp_data['Events']['Cause']]
            key_tip = 'parkinsonTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=ps_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
        return notifications
    
    def alzheimer_symptoms_tips(self):
        alzSymp_data = self.data['Alzheimer Symptoms']['analysis']
        as_dict = self.tipsFile[self.from_lang]['symptoms']['alzheimerSymptoms']
        notifications = None
        if alzSymp_data['Events'] is not None:
            info = as_dict['detection']
            cause = as_dict[alzSymp_data['Events']['Cause']]
            key_tip = 'alzheimerTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=as_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
        return notifications

    def launch_notification(self):
        symptoms_notifications = self.generate_tips()
        try:
            data = symptoms_notifications['General Symptoms'] 
            # 1) General Symptoms
            if data is not None and len(data)>0:
                # List of tuples
                for index, notif in enumerate(symptoms_notifications['General Symptoms']):
                    # Professional Notification
                    # Tuple of dictionaries
                    if index==0:
                        for profID, message in notif.items():
                            # create notification
                            for uuid_prof in self.professionals[profID]:
                                uuid_to = uuid_prof
                                content = message
                                priority = self.get_priority(module_name='Symptoms')
                                self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                       title='ICT4LIFE Symptoms report and Recommendation')
                    else:
                        # Patient Notification
                        for k, message in notif.items():
                            uuid_to = self.patientID
                            content = message
                            priority = self.get_priority(module_name='Symptoms')
                            self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                   title='ICT4LIFE Recommendation')
            # 2) Disease Symptoms
            if symptoms_notifications['Parkinson Symptoms'] is not None:
                data = symptoms_notifications['Parkinson Symptoms']
            elif symptoms_notifications['Alzheimer Symptoms'] is not None:
                data = symptoms_notifications['Alzheimer Symptoms']
            else:
                data = None

            if data is not None and len(data)>0:
                # List of tuples
                for index, notif in enumerate(data):
                    # Professional Notification
                    # Tuple of dictionaries
                    if index==0:
                        for profID, message in notif.items():
                            # create notification
                            for uuid_prof in self.professionals[profID]:
                                uuid_to = uuid_prof
                                content = message
                                priority = self.get_priority(module_name='Symptoms')
                                self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                       title='ICT4LIFE Symptoms report and Recommendation')
                    else:
                        # Patient Notification
                        for k, message in notif.items():
                            uuid_to = self.patientID
                            content = message
                            priority = self.get_priority(module_name='Symptoms')
                            self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                   title='ICT4LIFE Recommendation')
        except Exception as e:
            print(e)
            raise ValueError('An error occurred during the sending process of symptoms recommendations')

# =============================================================================
# =============================================================================
        
class MotionCognitiveNotification(Notification):
    def __init__(self, patient_information, professionals, data,
                 language, tipsFile):
        super().__init__(patient_information, professionals,
                         data, tipsFile, language)
        self.notification_schedule = {'Professionals': {'psychologistID':False,
                                                        'psychiatristID':False,
                                                        'neurologistID':False,
                                                        'physiotherapistID':True,
                                                        'caregiverID':True,
                                                        'socialWorkerID':True,
                                                        'dailyCenterWorkerID':True},
                                      'Patient':self.send_notification_patient()}
        
    def generate_tips(self):
        motionCogn_tips = { 'Motion Analysis': self.motion_tips(),
                            'Cognitive Analysis': self.cognitive_tips()}
        return motionCogn_tips
    
    def motion_tips(self):
        motion_data = self.data['Motion']
        motion_notifications = []
        if self.disease=='Parkinson':
            motion_dict = self.tipsFile[self.from_lang]['motionCognitive']['Motion']['parkinsonMotion']
        
            # Worry Level Analysis
            worry_level = motion_data['Worry Level']
            # Postural
            if worry_level['postural'] !=-1:
                info = motion_dict['postural']
                cause = motion_dict['worry level'] + str(worry_level['postural'])
                key_tip = 'motionParkinsonTips'
                notifications = self.obtain_personal_notification_from_file(data_dict=motion_dict,
                                                                            info=info,
                                                                            key_tip=key_tip,
                                                                            cause=cause)

                motion_notifications.append(notifications)
            # Balance
            if worry_level['balance'] !=-1:
                info = motion_dict['balance']
                cause = motion_dict['worry level'] + str(worry_level['balance'])
                key_tip = 'motionParkinsonTips'
                notifications = self.obtain_personal_notification_from_file(data_dict=motion_dict,
                                                                            info=info,
                                                                            key_tip=key_tip,
                                                                            cause=cause)
                motion_notifications.append(notifications)
            # Walking
            if worry_level['walking'] !=-1:
                info = motion_dict['walking']
                cause = motion_dict['worry level'] + str(worry_level['walking'])
                key_tip = 'motionParkinsonTips'
                notifications = self.obtain_personal_notification_from_file(data_dict=motion_dict,
                                                                            info=info,
                                                                            key_tip=key_tip,
                                                                            cause=cause)
                motion_notifications.append(notifications)
                
            # Involuntary Movement
            if worry_level['inv_movement'] !=-1:
                info = motion_dict['inv_movement']
                cause = motion_dict['worry level'] + str(worry_level['inv_movement'])
                key_tip = 'motionParkinsonTips'
                notifications = self.obtain_personal_notification_from_file(data_dict=motion_dict,
                                                                            info=info,
                                                                            key_tip=key_tip,
                                                                            cause=cause)
                motion_notifications.append(notifications)
            # Postural Changes
            if worry_level['postural_changes'] !=-1:
                info = motion_dict['postural_changes']
                cause = motion_dict['worry level'] + str(worry_level['postural_changes'])
                key_tip = 'motionParkinsonTips'
                notifications = self.obtain_personal_notification_from_file(data_dict=motion_dict,
                                                                            info=info,
                                                                            key_tip=key_tip,
                                                                            cause=cause)
                motion_notifications.append(notifications)
            # Movement Coordination
            if worry_level['movement_coord'] !=-1:
                info = motion_dict['movement_coord']
                cause = motion_dict['worry level'] + str(worry_level['movement_coord'])
                key_tip = 'motionParkinsonTips'
                notifications = self.obtain_personal_notification_from_file(data_dict=motion_dict,
                                                                            info=info,
                                                                            key_tip=key_tip,
                                                                            cause=cause)
                motion_notifications.append(notifications)
            # Rigidity
            if worry_level['rigidity'] !=-1:
                info = motion_dict['rigidity']
                cause = motion_dict['worry level'] + str(worry_level['rigidity'])
                key_tip = 'motionParkinsonTips'
                notifications = self.obtain_personal_notification_from_file(data_dict=motion_dict,
                                                                            info=info,
                                                                            key_tip=key_tip,
                                                                            cause=cause)
                motion_notifications.append(notifications)

        # -------------  ALZHEIMER -----------------
        elif self.disease=='Alzheimer':
            motion_dict = self.tipsFile[self.from_lang]['motionCognitive']['Motion']['alzheimerMotion']
                        
            # Reject the hypothesis that there is no a significant change
            if motion_data['Reject'] and motion_data['Change']=='decrease':
                info = motion_dict['detection']
                cause = motion_dict['statisticalAnalysis']
                key_tip = 'motionAlzheimerTips'
                notifications = self.obtain_personal_notification_from_file(data_dict=motion_dict,
                                                                            info=info,
                                                                            key_tip=key_tip,
                                                                            cause=cause)
                motion_notifications.append(notifications)
        return motion_notifications
    
    def cognitive_tips(self):
        self.notification_schedule = {'Professionals': {'psychologistID':False,
                                                        'psychiatristID':False,
                                                        'neurologistID':True,
                                                        'physiotherapistID':False,
                                                        'caregiverID':True,
                                                        'socialWorkerID':True,
                                                        'dailyCenterWorkerID':True},
                                      'Patient':self.send_notification_patient()}
        cognitive_data = self.data['Cognitive']
        cognitive_dict = self.tipsFile[self.from_lang]['motionCognitive']['Cognitive']
        cognitive_notifications = []
        
        # Short Memory
        if cognitive_data['ShortMemory']['Recommendation']:
            info = cognitive_dict['shortMemoryDetection']
            cause = cognitive_dict['scalesAnalysis']
            key_tip = 'cognitiveTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=cognitive_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
            cognitive_notifications.append(notifications)
        
        # Calculation
        if cognitive_data['Calculation']['Recommendation']:
            info = cognitive_dict['calculationDetection']
            cause = cognitive_dict['scalesAnalysis']
            key_tip = 'cognitiveTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=cognitive_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
            cognitive_notifications.append(notifications)
        
        # Recall
        if cognitive_data['Recall']['Recommendation']:
            info = cognitive_dict['recallDetection']
            cause = cognitive_dict['scalesAnalysis']
            key_tip = 'cognitiveTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=cognitive_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
            cognitive_notifications.append(notifications)
        
        return cognitive_notifications

    def launch_notification(self):
        """ Launch Motion & Cognitive Notifications """
        # 1) Generate notifications
        mot_cog_notifications = self.generate_tips()
        # Check if there is available data to notify
        try:
            data = mot_cog_notifications['Motion Analysis']
            # 1) Motion Notification
            if data is not None and len(data) > 0:
                for recommendation in data:
                    for index, notif in enumerate(recommendation):
                        if index==0:
                            # Professional Notification
                            # Tuple of dictionaries
                            for profID, message in notif.items():
                                # create notification
                                for uuid_prof in self.professionals[profID]:
                                    uuid_to = uuid_prof
                                    content = message
                                    priority = self.get_priority(module_name='MotionCognitive')
                                    self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                           title='ICT4LIFE Motion Recommendation')
                        else:
                            # Patient Notification
                            for k, message in notif.items():
                                uuid_to = self.patientID
                                content = message
                                priority = self.get_priority(module_name='MotionCognitive')
                                self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                       title='ICT4LIFE Motion Recommendation')
            # 2) Cognitive Notification
            data = mot_cog_notifications['Cognitive Analysis']
            if data is not None and len(data)> 0:
                # List of tuples
                for recommendation in data:
                    for index, notif in enumerate(recommendation):
                        if index==0:
                            # Professional Notification
                            # Tuple of dictionaries
                            for profID, message in notif.items():
                                # create notification
                                for uuid_prof in self.professionals[profID]:
                                    uuid_to = uuid_prof
                                    content = message
                                    priority = self.get_priority(module_name='MotionCognitive')
                                    self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                           title='ICT4LIFE Motion Recommendation')
                        else:
                            # Patient Notification
                            for k, message in notif.items():
                                uuid_to = self.patientID
                                content = message
                                priority = self.get_priority(module_name='MotionCognitive')
                                self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                       title='ICT4LIFE Motion Recommendation')
        except Exception as e:
            print(e)
            raise ValueError('An error occurred during the sending procedure of motion & cognitive recommendations')
        return True
    
# =============================================================================
# =============================================================================
        
class TrainingNotification(Notification):
    def __init__(self, patient_information, professionals, data,
                 language, tipsFile):
        super().__init__(patient_information, professionals,
                         data, tipsFile, language)
        self.notification_schedule = {'Professionals': {'psychologistID':False,
                                                        'psychiatristID':False,
                                                        'neurologistID':False,
                                                        'physiotherapistID':True,
                                                        'caregiverID':True,
                                                        'socialWorkerID':True,
                                                        'dailyCenterWorkerID':True},
                                      'Patient':self.send_notification_patient()}
    def generate_tips(self):
        training_tips = {'Training Analysis': self.training_tips()}
        return training_tips
    
    def training_tips(self):
        training_data = self.data['Training Data']
        training_dict = self.tipsFile[self.from_lang]['training']
        training_notifications = []
        
        # Physiotherapy documentation
        if training_data['PhysioDoc']['Recommendation']:
            self.notification_schedule = {'Professionals': {'psychologistID':False,
                                                            'psychiatristID':False,
                                                            'neurologistID':False,
                                                            'physiotherapistID':True,
                                                            'caregiverID':True,
                                                            'socialWorkerID':True,
                                                            'dailyCenterWorkerID':True},
                                      'Patient':self.send_notification_patient()}
            info = training_dict['physiotherapyDoc']
            cause = training_dict['scalesAnalysis']
            key_tip = 'trainingTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=training_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
            training_notifications.append(notifications)
        # Speech Theory documentation
        if training_data['SpeechDoc']['Recommendation']:
            self.notification_schedule = {'Professionals': {'psychologistID':True,
                                                            'psychiatristID':False,
                                                            'neurologistID':False,
                                                            'physiotherapistID':False,
                                                            'caregiverID':True,
                                                            'socialWorkerID':True,
                                                            'dailyCenterWorkerID':True},
                                      'Patient':self.send_notification_patient()}
            
            info = training_dict['SpeechDoc']
            cause = training_dict['scalesAnalysis']
            key_tip = 'trainingTips'
            notifications = self.obtain_personal_notification_from_file(data_dict=training_dict,
                                                                        info=info,
                                                                        key_tip=key_tip,
                                                                        cause=cause)
            training_notifications.append(notifications)
        return training_notifications

    def launch_notification(self):
        """ Launch Training Notifications """
        # 1) Generate notifications
        training_notifications = self.generate_tips()
        # Check if there is available data to notify
        try:
            data = training_notifications['Training Analysis']
            if data is not None and len(data)>0:
                for doc in data:
                    for index, notif in enumerate(doc):
                        if index==0:
                            # Professional Notification
                            for profID, message in notif.items():
                                # create notification
                                for uuid_prof in self.professionals[profID]:
                                    uuid_to = uuid_prof
                                    content = message
                                    priority = self.get_priority(module_name='Training')
                                    self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                           title='ICT4LIFE Training Recommendation')
                        else:
                            # Patient Notification
                            for k, message in notif.items():
                                uuid_to = self.patientID
                                content = message
                                priority = self.get_priority(module_name='Training')
                                self.send_notification(priority=priority, uuid_to=uuid_to, content=content,
                                                       title='ICT4LIFE Training Recommendation')
        except Exception as e:
            print(e)
            raise ValueError('An error occurred during the sending procedure of training recommendations')
        return True
    
    