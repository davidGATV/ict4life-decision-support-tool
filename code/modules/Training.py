# ======================================================================================================================
# ---------------------------------------------- TRAINING MODULE -------------------------------------------------------
# ======================================================================================================================
from utils.util import getTrainingDocID,check_disease_phase

class Training():
    def __init__(self, training_parameters):

        # Variables and paramenters
        self.scales = training_parameters['Scales']
        self.patientName = training_parameters['Name']
        self.patientID = training_parameters['ID']
        self.disease = training_parameters['Disease']
        self.disease_phase = training_parameters['Disease Phase']

        # Scales paramenters
        self.hoehnYard = self.scales[0]
        self.SPMSQ = self.scales[1]
        self.MMSE = self.scales[2]
        self.PDCRS = self.scales[3]
        self.TinettiBergAPM = self.scales[4]
        self.UPDRS = self.scales[5]
        self.APMrigidity = self.scales[6]
        self.PDQ8 = self.scales[7]
        self.lawton = self.scales[8]

        self.all_training_docs = training_parameters['Catalog'].get_all_forms()

    # ==================================================================================================================
    def run(self):
        print(100 * '-')
        print('3.1) Extracting training documents')
        print(100 * '-')
        training_tips,document_list = self.analyse_training()

        return training_tips,document_list

    # ==================================================================================================================
    def analyse_training(self):

        # 1) Physiotherapy documents
        apm_result  = self.APMrigidity['result']
        training = {'PhysioDoc':{'Priority':None,
                                 'Recommendation':False},
                    'SpeechDoc':{'Priority':None,
                                 'Recommendation':False}}
    
        add_physio_doc = False
        add_speech_doc = False
        priority_ph = None
        priority_sp = None
        # split the total score of the apm rigidity into 3 classes: mild, moderate and severe.
        threshold = 15
        documents_recommended_list = []
        # Mild
        if check_disease_phase(self.disease, self.disease_phase)==1 and apm_result>threshold:
            add_physio_doc = True
            priority_ph = 'MEDIUM'
            # Patient
            doc_name = "PHYSIO_PT_MILD"
            doc_uuid = getTrainingDocID(self.all_training_docs, doc_name)
            documents_recommended_list.append(doc_uuid)

        elif check_disease_phase(self.disease, self.disease_phase)==2 and apm_result>threshold:
            add_physio_doc = True
            priority_ph='MEDIUM'
            doc_name = "PHYSIO_PT_MODERATE"
            doc_uuid = getTrainingDocID(self.all_training_docs, doc_name)
            documents_recommended_list.append(doc_uuid)

        elif check_disease_phase(self.disease, self.disease_phase) == 3 and apm_result > threshold:
            add_physio_doc = True
            doc_name = "PHYSIO_PT_SEVERE"
            doc_uuid = getTrainingDocID(self.all_training_docs, doc_name)
            documents_recommended_list.append(doc_uuid)

        # 1) Logopedia documents
        # 1.1 Language

        if check_disease_phase(self.disease, self.disease_phase)== 1 and self.MMSE['parts'][4]<8:
            add_speech_doc = True
            priority_sp = 'HIGH'
            doc_name = "LOG_PT_WRITE"
            doc_uuid = getTrainingDocID(self.all_training_docs, doc_name)
            documents_recommended_list.append(doc_uuid)

            doc_name = "LOG_PT_SPEAK"
            doc_uuid = getTrainingDocID(self.all_training_docs, doc_name)
            documents_recommended_list.append(doc_uuid)

            doc_name = "LOG_DYS_PT_MILD"
            doc_uuid = getTrainingDocID(self.all_training_docs, doc_name)
            documents_recommended_list.append(doc_uuid)


        elif check_disease_phase(self.disease, self.disease_phase==2) and self.MMSE['parts'][4]<6:
            add_speech_doc = True
            priority_sp = 'MEDIUM'
            doc_name = "LOG_PT_WRITE"
            doc_uuid = getTrainingDocID(self.all_training_docs, doc_name)
            documents_recommended_list.append(doc_uuid)

            doc_name = "LOG_PT_SPEAK"
            doc_uuid = getTrainingDocID(self.all_training_docs, doc_name)
            documents_recommended_list.append(doc_uuid)

            doc_name = "LOG_DYS_PT_MODERATE"
            doc_uuid = getTrainingDocID(self.all_training_docs, doc_name)
            documents_recommended_list.append(doc_uuid)


        # 1.2 Dysphagia
        if add_physio_doc:
            #print('Only Physiotherapy document')
            training['PhysioDoc']['Priority'] = priority_ph
            training['PhysioDoc']['Recommendation'] = True

        if add_speech_doc:
            #print('Speech Therapy docs')
            training['SpeechDoc']['Priority'] = priority_sp
            training['SpeechDoc']['Recommendation'] = True
            
        return training,documents_recommended_list


    def generate_output(self):
        # Run the Model
        training,documents_recommended_list =  self.run()
        
        # Generate the output of the model
        training_output = {'Training Data':training,
                           'Documents':documents_recommended_list}
        return training_output
