# ======================================================================================================================
# -------------------------------------------- Statistics Library ------------------------------------------------------
# # ======================================================================================================================

from math import log
import numpy as np
from scipy.stats import ranksums

def sigmoid(x):
    return (1 / (1 + np.exp(-x)))

def compute_bayes_theorem( prior, likelihood, evidence):
    """ Compute Bayes theorem """
    posterior = (likelihood * prior) / evidence
    return posterior

def get_prior_probability(events):
    """ Compute the prior probability of an event count the values of a given list which are higher
        that the median."""
    prob = len([i for i in events if i >= np.median(events)]) / len(events)
    return prob

def solve_total_probability(A, alpha, beta, gamma):
    """ Solve the equation 1 = A + alpha*X + beta*Y + gamma*Z  where X,Y,Z are posterior probabilities and
    we assume that X=Y and Z =1.5X """

    X = (1-A)/(alpha + beta + 1.5*gamma)
    Y = X
    Z = 1.5*X

    return X,Y,Z

def compute_evidence_probability(events, alpha=0.3):

    # Remove -1 from events
    events = [x for x in events if x >0]
    # Unnormalize weights
    un_weights = [0.5 * (1 - alpha * log(i + 1)) for i in range(len(events))]
    # Normalize weights
    weights = un_weights / np.sum(un_weights)
    weights = weights[::-1]

    # Compute probability
    prob_evidence = np.sum(np.array([events]) * weights)

    if any(i >= 1 for i in events):
        prob_evidence = sigmoid(prob_evidence)

    return prob_evidence

def log_prob( threshold, alpha=0.3):
    """ Compute the equation threshold*(1 - alpha*log(threshold)) """
    return threshold*(1 - alpha*log(threshold))

def compute_prior_probability_binary_variable(variable, threshold):

    if isinstance(variable, list) or variable >1:
        raise ValueError('The variable is not discrete or binary.')
    else:
        if variable == 1:
            prior_prob  = log_prob(threshold=threshold)
        else:
            prior_prob = 1 - log_prob(threshold=threshold)

    return prior_prob


def maximum_a_posteriori_decisor(posterior_probabilitites):


    posterior_prob = posterior_probabilitites['probabilitites']
    posterior_causes = posterior_probabilitites['causes']

    index = np.argmax(posterior_prob)
    probability = posterior_prob[index]
    cause = posterior_causes[index]

    return cause, probability

def maximum_likelihood_decisor(likelihoods_dictionary):
    """ Compute the Maximum Likelihood method and returns both the cause and the associated probability.
    """

    likelihood_prob = likelihoods_dictionary['probabilities']
    likelihood_cause = likelihoods_dictionary['causes']
    # Compute the maximum
    index = np.argmax(likelihood_prob)
    max_prob = likelihood_prob[index]
    probability = max_prob

    # Get the cause
    cause_var = likelihood_cause[index]
    return cause_var, probability

def compute_wilcoxon_test(data, sd_data, alpha = 0.05):
    # 2) Wilcoxon test to find significance in the probability
    reject_ho = False
    # Wilcoxon rank-sum test for non-normal distributed samples

    # Remove missing data (-1)
    data = [x for x in data if x >=0]
    if np.var(data) > 0:
        threshold = round(len(data) / 2)
        data1 = data[:threshold]
        data2 = data[threshold:]

        # Compute test
        stat, p = ranksums(data1, data2)
        if p > alpha:
            print('Same distribution (fail to reject H0)')
        else:
            print('Different distribution (reject H0)')
            reject_ho = True

        # Generate output
        if (sd_data <=0 and reject_ho):
            # Significant decrease change in movement
            cause = 'decrease'
        elif sd_data>=0 and reject_ho:
            cause = 'increase'
        else:
            cause=None
    else:
        p=None
        cause =None

    output = {'p_value':p,
              'Change':cause,
              'Reject':reject_ho}
    return output

