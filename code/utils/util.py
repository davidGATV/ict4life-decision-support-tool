# =============================================================================
# -------------------------- UTIL LIBRARY FOR DST -----------------------------
# =============================================================================

# -*- coding: UTF-8 -*-

#import simplejson as json
import json
import os
import itertools
from itertools import groupby
from datetime import datetime, timedelta
from lib import S3FileManager

# ======================================================================================================================
def parseJsonToDict(json_file):
    """ Generate a dictionary from a Json file.
        INPUTS:   json File
        OUTPUTS:  Python Dictionary object """
    # Avoid errors in the parser
    filename, extension = os.path.splitext(json_file)
    try:
        # Original extension
        with open(json_file, 'r', encoding='utf-8') as data_file:
            data = json.load(data_file, encoding="utf-8")
            input_data = data
    except Exception:
        extension_u = extension.upper()
        json_file = filename + extension_u
        with open(json_file, 'r', encoding='utf-8') as data_file:
            data = json.load(data_file, encoding="utf-8")
            input_data = data
    return input_data
# ======================================================================================================================
def check_disease_phase(disease, disease_phase):
    state = 0
    # Alzheimer
    if disease == 'Alzheimer':
        if disease_phase == 'Etapa 1' or disease_phase == 'Etapa 2':
            state = 1
        elif disease_phase == 'Etapa 3' or disease_phase== 'Etapa 4' or disease_phase == 'Etapa 5':
            state = 2
        else:
            state = 3
    # Parkinson
    else:
        if disease_phase == 'Estadio 1' or disease_phase== 'Estadio 2':
            state = 1
        elif disease_phase == 'Estadio 3' or disease_phase== 'Estadio 4':
            state = 2
        else:
            state = 3
    return state

# ======================================================================================================================
def get_language(nationality):
    language = ''
    if nationality == 'ES':
        language='ES'
    elif nationality == 'EN':
        language = 'EN'
    elif nationality == 'FR':
        language = 'FR'
    else:
        language = 'HU'
    return language

# ======================================================================================================================
def dataConverter(o):
    if isinstance(o, datetime):
        return o.__str__()
# ======================================================================================================================
def generate_dst_output(output_filename, content, user):
    try:
        current_data = {}
        # Check file
        if os.path.exists(output_filename):
            # Update
            with open(output_filename, "r+") as jsonFile:
                current_data = json.load(jsonFile, encoding="utf-8")
            current_data[user] = content
        else:
            # New file
            current_data[user] = content
        with open(output_filename, 'w') as outfile:
            json.dump(current_data, outfile, indent=4, sort_keys=True,
                       default = dataConverter)
            outfile.close()
    except Exception as e:
        print(e)

# ======================================================================================================================
def check_medicationChange(medicationChange):
    if 1 in medicationChange:
        return True
    else:
        return False    
# ======================================================================================================================

def have_mental_disorder(comorbidities, GDS_SF=None,disease='Alzheimer'):
    """ This method returns true if the patient suffers from depression,
    anxiety or another mental disorder"""
    have_depression = False

    if disease=='Alzheimer':
        if GDS_SF != None and GDS_SF>7:
            have_depression = True
    if 'Enfermedades o trastornos mentales' in comorbidities:
        have_depression = True

    return have_depression
# ======================================================================================================================

def have_addiction_disorder(comorbidities):
    have_addiction_dis = False

    if 'Consumo de alcohol/drogas y trastornos orgánicos mentales inducidos por alcohol/drogas' in comorbidities:
        have_addiction_dis = True

    return have_addiction_dis

# ======================================================================================================================
def decodeComorbidities():
    """ Create a dictionary with the different comorbidities. Returns a 
        Python dictionary"""
    comorbidities = {}
    comorbidities[1]  = "NSC"  # "Enfermedades y trastornos del sistema nervioso"  
    comorbidities[2]  = "SC"   # "Enfermedades y trastornos del ojo, oido, nariz, boca y garganta"   
    comorbidities[3]  = "RSC"  # "Enfermedades y trastornos del sistema respiratorio"  
    comorbidities[4]  = "CSC"  # "Enfermedades y trastornos del sistema circulatorio" 
    comorbidities[5]  = "DSC"  # "Enfermedades y trastornos del sistema digestivo" 
    comorbidities[6]  = "HSC"  # "Enfermedades y trastornos del sistema hepatobilar y pancreas" 
    comorbidities[7]  = "MSSC" # "Enfermedades y trastornos del sistema musculoesqueletico y tejido conectivo" # 
    comorbidities[8]  = "DC"   # "Enfermedades y trastornos de la piel, del tejido subcutaneo y de la mama" 
    comorbidities[9]  = "ESC"  # "Enfermedades y trastornos endocrinos, nutricionales y metabolicos" 
    comorbidities[10] = "USC"  # "Enfermedades y trastornos del rinon y vias urinarias" 
    comorbidities[11] = "MRSC" # "Enfermedades y trastornos del sistema reproductor masculino" 
    comorbidities[12] = "FRSC" # "Enfermedades y trastornos del sistema reproductor femenino" 
    comorbidities[13] = "BSC"  # "Enfermedades y trastornos de la sangre, del sistema hematopoyetico y del sistema inmunitario"
    comorbidities[14] = "RC"   # "Enfermedades y trastornos reumaticos"  
    comorbidities[15] = "MC"   # "Enfermedades y trastornos mentales (neurologicos y psiquiatricos)" 
    comorbidities[16] = "DAC"  # "Consumo de alcohol/drogas y trastornos organicos mentales inducidos por alcohol/drogas"
    comorbidities[17] = "CC"   # "Cancer" 
    comorbidities[18] = "OC"   # "Otros"  
    return comorbidities

# ======================================================================================================================

def decodeParkinsonMedication():
    """ Create a dictionary with the different Parkinson medication. Returns 
        a python dictionary. """
    parkinson_medication = {}
    parkinson_medication[1] = 'SINEMET'
    parkinson_medication[2] = 'SINEMET RETARD'
    parkinson_medication[3] = 'SINEMET PLUS'
    parkinson_medication[4] = 'SINEMET PLUS RETARD'
    parkinson_medication[5] = 'STALEVO' 
    parkinson_medication[6] = 'MADOPAR'
    parkinson_medication[7] = 'MADOPAR RETARD'
    parkinson_medication[8] = 'ACTISON'
    parkinson_medication[9] = 'DUODOPA'
    parkinson_medication[10] = 'VIROSOL'
    parkinson_medication[11] = 'SYMADINE'
    parkinson_medication[12] = 'SYMMETREL'
    parkinson_medication[13] = 'AMANTADINE'
    parkinson_medication[14] = 'AKINETON'
    parkinson_medication[15] = 'AKINETON RETARD'
    parkinson_medication[16] = 'TREMARIL'
    parkinson_medication[17] = 'KEMADREN'
    parkinson_medication[18] = 'ARTANE'
    parkinson_medication[19] = 'ARTANE RETARD'
    parkinson_medication[20] = 'PLURIMEN'
    parkinson_medication[21] = 'AZILECT'
    parkinson_medication[22] = 'COMTAN'    
    parkinson_medication[23] = 'TASMAR'
    parkinson_medication[24] = 'APOMORFINA'
    parkinson_medication[25] = 'PARLODEL'
    parkinson_medication[26] = 'LACTISMINE'
    parkinson_medication[27] = 'SOGILEN'
    parkinson_medication[28] = 'DOPERGIN'
    parkinson_medication[29] = 'PHARKEN'   
    parkinson_medication[30] = 'REQUIP'
    parkinson_medication[31] = 'MIRAPEXIN'
    parkinson_medication[32] = 'NEUPRO'
    parkinson_medication[33] = 'ROPINIROL'
    parkinson_medication[34] = 'ESTIMULACION CEREBRAL'
    parkinson_medication[35] = 'OTROS'
    return parkinson_medication

# ======================================================================================================================
def decodeAlzheimerMedication():
    alzheimer_medication = {}
    alzheimer_medication[1] = 'ARICEPT'
    alzheimer_medication[2] = 'PALIXID'
    alzheimer_medication[3] = 'DONESYN'
    alzheimer_medication[4] = 'DONECEPT'
    alzheimer_medication[5] = 'EXELON TABLETTE' 
    alzheimer_medication[6] = 'RIVASTIGMINE TRANSDERMAL PATCH'
    alzheimer_medication[7] = 'NIVALIN'
    alzheimer_medication[8] = 'RAZADYNE'
    alzheimer_medication[9] = 'REMINYL'
    alzheimer_medication[10] = 'LYCOREMINE'
    alzheimer_medication[11] = 'MIRVEDOL'
    alzheimer_medication[12] = 'EBIXA'
    alzheimer_medication[13] = 'CAVINTON'
    alzheimer_medication[14] = 'NOOTROPIL'
    alzheimer_medication[15] = 'LUCETAM'
    alzheimer_medication[16] = 'MEMORIL'
    alzheimer_medication[17] = 'TRENTAL'
    alzheimer_medication[18] = 'CHINOTAL'
    alzheimer_medication[19] = 'SERMION'
    alzheimer_medication[20] = 'GINGIUM'
    alzheimer_medication[21] = 'JUMEX'
    alzheimer_medication[22] = 'VITAMIN E'
    return alzheimer_medication

# ======================================================================================================================

def getMessageContent(d, moduleName):
    message = {}
    message[moduleName] = {}
    message_information = {}
    # Check if the dictionary is empty
    if d:
        for k, v in d.items():
            if isinstance(v, dict):                
                if v:
                    for caregiver, content in v.items():
                        # Initialise content
                        if not caregiver in message_information:
                            message_information[caregiver] = {}
                        if not k in message_information[caregiver]:
                            message_information[caregiver][k] = {}
                        
                        message_information[caregiver][k]['type'] = k
                        message_information[caregiver][k]['priority'] = content[0]
                        message_information[caregiver][k]['content'] = content[1]
    
    message[moduleName] = message_information
    return message


def getTrainingDocID(all_training, description):
    tr_id = ''
    for index, element in enumerate(all_training['trainings']):
        if index >=1:
            if element['descrip'] == description:
                tr_id = element['uuid']
                break
    if tr_id=='':
        raise ValueError("There is not any training doc with such description")

    return tr_id


def parse_probabilities(probabilities, name):
    do_val = False
    prob_val = -1
    for p in probabilities:
        if not do_val:
            for k,v in p.items():
                if k=='type':
                    if v==name:
                        do_val = True
                if k=='value' and do_val:
                    prob_val = v
                    break
    return prob_val

def check_events(events, dates):
    remain_events=[]
    remain_dates = []
    for index, event in events:
        if event >=0:
            date = dates[index]
            remain_dates.append(date)
            remain_events.append(event)
    return remain_dates, remain_events

def replaceMissingEvents(events):
    new_events = []
    for event in events:
        if event <0:
            event = 0
        new_events.append(event)
    return new_events

def get_period(dates):
    # Convert the string date to a datetime object
    d1 = datetime.strptime(dates[0], '%Y-%m-%d').date()
    d2 = datetime.strptime(dates[1], '%Y-%m-%d').date()
    delta = d2 - d1
    period = []
    # Create the dates of the analysis using the start and the end date.
    for i in range(delta.days):
        # print(d1 + timedelta(days=i))
        period.append(d1 + timedelta(days=i))
    return period


def detect_missing_data(event, dates, threshold=5):
    b = range(len(event))
    event_cleaned = []
    dates_cleaned = []
    try:
        # Loop over to find zeros
        for group in groupby(iter(b), lambda x: event[x]):
            if group[0] == 0:
                val = list(group[1])
                # Check if the number of zeros is significant
                diff = int(max(val) - min(val))
                if diff >= threshold:
                    zero_indx = list(range(min(val), min(val) + diff))
                    event_cleaned = [val for indx, val in enumerate(event) if indx not in zero_indx]
                    dates_cleaned = [date for indx, date in enumerate(dates) if indx not in zero_indx]
    except Exception as e:
        print(e)
        print('Unable to clean event')

    return event_cleaned, dates_cleaned


def generate_data_visualization_files(label='charts', uuid=None, dates=None, data=None,
                                      output_path=None, input_path=None,output_data_path=None):
    error = True
    data_total = []
    try:
        if label=='correlation':
            # Add dates
            data[0]['dates'] = [dates[0] + '-' + dates[1]]
            filename_output = str(uuid) + '_events_' + label + '.json'
        else:
            filename_output = str(uuid) + '_predictions_' + label + '.json'

        local_path_output = os.path.join(output_data_path, filename_output)

        if data is not None:
            data_total.append(data)
            # Check if exists
            if os.path.exists(local_path_output):
                # Update
                with open(local_path_output, "r+") as jsonFile:
                    current_data = json.load(jsonFile, encoding="utf-8")

                data_total +=current_data

            # Create JSON File and safe into visualization
            with open(local_path_output, 'w') as outfile:
                json.dump(data_total, outfile, indent=4, sort_keys=True,
                           default = dataConverter)
                outfile.close()
            error = False

        # Upload File
        if not error:
            upload_file_cloud(output_path, input_path, output_data_path, filename_output)
    except Exception as e:
        print(e)
        print('An error occurred during the data visualization file generation')
    return error


def downcload_file_cloud(output_path, input_path, local_path_input):
    success = 0
    try:
        # FileManager Constructor
        fileManager = S3FileManager.S3FileManger(output_path, input_path)
        # Download the last version in the local path
        success = fileManager.latest_document(local_path_input)
    except Exception as e:
        print(e)
        print('Unable to download files from the Cloud')
    return success

def upload_file_cloud(output_path, input_path, local_path_output, filename_output):
    success = 0
    try:
        # FileManager Constructor
        fileManager = S3FileManager.S3FileManger(output_path, input_path)

        # Upload file to the server
        fileManager.upload_file(local_path_output, filename_output)

    except Exception as e:
        print(e)
        print('Unable to upload file {} to the Cloud'.format(filename_output))
    return success

# ======================================================================================================================
# ----------------------------------------------- END UTILS ------------------------------------------------------------
# ======================================================================================================================