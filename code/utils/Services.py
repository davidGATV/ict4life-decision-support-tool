# -*- coding: utf-8 -*-
"""
Created on Wed Sep  5 15:48:55 2018

@author: user
"""

import requests
import json
from utils.util import parse_probabilities

# %% SERVICES CLASS TO GET THE DATA REQUIRED FOR THE DST
class Services():
    def __init__(self, input_data,):
        self.input_data = input_data
        self.access_token, self.uuidUser = self.get_credentials()
        self.access_token, self.uuidUser = self.get_credentials()

    # ======================================================================
    # ================ CONNECT TO ICT4LIFE SERVICES ========================
    # ======================================================================

    def login(self):
        """ Login to the Platform in order to access to the required data."""
        try:
            print("Login to ICT4Life Services ... ")
            self.url = 'https://ict4life-services.articatelemedicina.com/artica-uno-login/loginOauth/login/'
            self.headers = {'Accept': 'application/json' ,'Content-Type': 'application/json'}
            self.body = {"userName": "decisionsupporttool@ict4life.eu",
                    "password": "123456789"}
            self.r = requests.post(self.url, data=json.dumps(self.body),headers=self.headers)
        except:
            raise ValueError('Impossible to connect to the ICT4Life Services.')
            
        return  self.r.json()
    
    def get_credentials(self):
        try:
            request = self.login()
            self.access_token = request['token']
            self.uuidUser = request['uuidUser']
        except Exception as e:
            print(e)
            print('Unable to get credentials')
        return self.access_token, self.uuidUser
    
    def call_services(self, url, headers, type ='get'):
        
        if type == 'get':
            self.r = requests.get(url, headers=headers)
        elif type == 'post':
            self.r = requests.post(url, data=json.dumps(self.body),
                              headers=headers)
        return self.r.json()
    
    # ======================================================================
    # ======================================================================
    # ======================================================================

    # ======================================================================
    # ==================== GET PATIENT INFORMATION =========================
    # ======================================================================

    def get_patientName(self, patientID):
        """ Returns a string with the name of the patient."""
        # Parameters for getting the patient Name
        patientNameHeaders = {'uuidUser': patientID, 'access_token': self.access_token,
                                   'Accept': 'application/json', 'Content-Type': 'application/json'}
        patientNameURl = 'https://ict4life-services.articatelemedicina.com/artica-uno-personas/admin/person/master/'

        patientInfoJson = self.call_services(patientNameURl, patientNameHeaders, type='get')
        name = patientInfoJson['name']
        return name

    def get_patient_information(self):
        """ Returns the personal information for a patient including:
            - Name
            - ID
            - Nationality
            - Associated disease (Alzheimer or Parkinson)
            - Disease phase
            - Associated Comorbidities"""

        patientID = self.input_data['personalInformation']['patientID']
        patientName = self.get_patientName(patientID)

        language = self.input_data['personalInformation']['nationality']
        disease,disease_phase = self.get_disease()    
        comorbidities = self.get_comorbidities()
        
        return patientName, patientID, language, disease, disease_phase, comorbidities

    def get_disease(self):

        """ Returns two strings:
         - Disease Name
         - Disease Phase """

        self.disease_uuid = self.input_data['personalInformation']['disease']
        self.disease_phase_uuid = self.input_data['personalInformation']['diseasePhase']
        
        self.url = 'https://ict4life-services.articatelemedicina.com/artica-uno-tipos/admin/catalog/?tables=DISEASE'
        self.headers = {'Accept': 'application/json' ,'Content-Type': 'application/json',
                   'Accept-Language':'es', 'access_token': self.access_token,
                   'uuidUser': self.uuidUser}
        self.type ='get'
        
        # Call the ICT4Life Services
        disease_json = self.call_services(self.url, self.headers, type='get')['diseases']
        
        # Check the kind of disease associated to the patient
        self.disease = ''
        self.disease_phase = ''
        try:
            for item in disease_json:
                for key, value in item.items():
                    if key == 'uuid':
                        if value == self.disease_uuid:
                             self.disease = item['descrip']
                    if key == 'subType':
                        for sub_item in value:
                            for sub_k, sub_v in sub_item.items():
                                if sub_k == 'uuid':
                                    if sub_v == self.disease_phase_uuid:
                                        self.disease_phase = sub_item['descrip']
        except ValueError as e:
            print(e.args)
        return self.disease, self.disease_phase

    def get_comorbidities(self):
        
        self.comorbidities_uuid = self.input_data['personalInformation']['comorbidities']
        
        self.url = 'https://ict4life-services.articatelemedicina.com/artica-uno-tipos/admin/catalog/?tables=CORMO'
        self.headers = {'Accept': 'application/json' ,'Content-Type': 'application/json',
                        'Accept-Language':'es', 'access_token': self.access_token,
                        'uuidUser': self.uuidUser}
        self.type ='get'
        
        # Call the ICT4Life Services
        comor_json = self.call_services(self.url, self.headers, type='get')['comorbidities']
        
        comorbidities = []
        for comorbidity in comor_json:
            for key, value in comorbidity.items():
                    if key == 'uuid':
                        if value in self.comorbidities_uuid:
                             comorbidities.append(comorbidity['descrip'])
        return comorbidities
    
    def get_date(self):
        return (self.input_data['startDate'], self.input_data['endDate'])

    def get_professionals(self):
        professionals_list = []
        all_professionals = []
        if 'professionals' in self.input_data.keys():
            professionals_list = self.input_data['professionals']

            all_professionals = ["psychologistID", "psychiatristID",
                                      "neurologistID", "physiotherapistID",
                                      "caregiverID", "socialWorkerID",
                                      "dailyCenterWorkerID", "caregiverID2",
                                      "caregiverID3"]
        return all_professionals, professionals_list


    def get_scales(self):
        """ Get the scales associated to a patient."""
        n_scales = 10
        scales = [-1 for x in range(n_scales)]
        try:
            HoehnYard = self.input_data['scales'][0]['HoehnYard']
            MMSE      = self.input_data['scales'][0]['MMSE']
            SPMSQ     = self.input_data['scales'][0]['SPMSQ']
            PDCRS     = self.input_data['scales'][0]['PDCRS']
            TinettiBergAPM = self.input_data['scales'][0]['TinettiBergAPM']
            UPDRS          = self.input_data['scales'][0]['UPDRS']
            APMrigidity    = self.input_data['scales'][0]['APMrigidity']
            PDQ8           = self.input_data['scales'][0]['PDQ8']
            lawton         = self.input_data['scales'][0]['lawton']
            GDSSF          = self.input_data['scales'][0]['GDSSF']
            scales = [HoehnYard, SPMSQ, MMSE, PDCRS, TinettiBergAPM, UPDRS, PDQ8, lawton, GDSSF, APMrigidity]
        except Exception as e:
            print(e)
            print('Unable to extract the scales from the input')

        return scales

    def get_medical_information(self):
        """ Returns a list with the associated medication"""
        if 'medication' in self.input_data.keys():
            medication = self.input_data['medication']
        else:
            medication = None
        return medication

    def get_movement_evolution_information(self):
        if 'movementEvaluations' in self.input_data:
            return self.input_data['movementEvaluations']
        else:
            return None

    def get_probabilities(self):
        prob = None
        try:
            prob = self.input_data['probabilities']
        except Exception as e:
            print(e)
            print('Unable to get probabilities')
        return prob
       
        
    def get_events(self, name=None):
        event = None
        if name is not None:
            try:
                event = self.input_data[name]['events']
            except Exception as e:
                print(e)
                print('Unable to extract event: ', name)
        return event
    def get_events_deviation(self, name=None):
        sd = None
        if name is not None:
            try:
                sd = self.input_data[name]['result']
            except Exception as e:
                print(e)
                print('Unable to extract deviation of event: ', name)
        return sd
        
    def get_information_by_name(self, name):
        info = None
        try:
            info = self.input_data[name]
        except Exception as e:
            print(e)
            print('Unable to extract data: ',name)
        return info

    def get_gsr_statistics(self, stat='mode'):
        statistics = {'mode', 'mean', 'kurtosis', 'skewness', 'min', 'max',
                      'median'}
        if stat not in statistics:
            return None
        else:
            key = stat + 'GSR'
            gsr_statistic = self.input_data['galvanic skin response'][key]
            return gsr_statistic
    
    def get_heartRate_statistics(self, stat='mode'):
        statistics = {'mode', 'mean', 'kurtosis', 'skewness', 'min', 'max',
                      'median'}
        if stat not in statistics:
            return None
        else:
            key = stat + 'HeartRate'
            hr_statistic = self.input_data['heart_rate'][key]
            return hr_statistic
        
    def  get_all_forms(self):
        """ Get all Training forms from the Services"""
        training_json = None
        try:
            self.trainingURL = 'https://ict4life-services.articatelemedicina.com/artica-uno-tipos/admin/catalog/?tables=TRAINING'
            self.trainingHeaders = {'Accept': 'application/json' ,
                                    'Content-Type': 'application/json',
                                    'Accept-Language':'es',
                                    'access_token': self.access_token,
                                    'uuidUser': self.uuidUser}

            training_json = self.call_services(self.trainingURL,
                                               self.trainingHeaders,
                                               type='get')
        except Exception as e:
            print(e)
            print('Unable to extract training documents from ICT4LIFE servicess')
        return training_json
    # =========================================================================
    # ========================= GET SYMPTOMS ==================================
    # =========================================================================


    def parkinson_symptoms_probabilities(self):
        probabilities = self.get_probabilities()
        parkinson_events_params = {'Events':[]}
        if probabilities is not None:
            self.p_parkEvent_festination = parse_probabilities(probabilities, 'ParkinsonsEvents|festination')
            self.p_parkEvent_fallDown    = parse_probabilities(probabilities, 'ParkinsonsEvents|fallDown')
            self.p_parkEvent_lossBalance = parse_probabilities(probabilities, 'ParkinsonsEvents|fallDown')
            self.p_parkEvent_MedChange = parse_probabilities(probabilities, 'ParkinsonsEvents|medicationChange')
            self.p_parkEvent_freezing = parse_probabilities(probabilities, 'ParkinsonsEvents|freezing')
            self.p_parkEvent = parse_probabilities(probabilities, 'ParkinsonsEvents')

            parkinson_events_params['Events']= [self.p_parkEvent, self.p_parkEvent_fallDown,
                                                self.p_parkEvent_festination, self.p_parkEvent_freezing,
                                                self.p_parkEvent_lossBalance, self.p_parkEvent_MedChange]

        return parkinson_events_params


    def alzheimer_symptoms_probabilities(self):
        probabilities = self.get_probabilities()
        alzheimer_symptoms_params = {'Confusion':[]}
        if probabilities is not None:
            # Confusion Model
            self.p_confusion_ABE = parse_probabilities(probabilities, 'confusion|abnormalEvents')
            self.p_confusion = parse_probabilities(probabilities, 'Confusion')
            self.p_confusion_neuCom = parse_probabilities(probabilities, 'confusion|neurologicComorbidites')
            self.p_confusion_psychiaCom = parse_probabilities(probabilities, 'confusion|psychiatricComorbidites')

            alzheimer_symptoms_params['Confusion']= [self.p_confusion, self.p_confusion_ABE, self.p_confusion_neuCom,
                                                       self.p_confusion_psychiaCom]
        return alzheimer_symptoms_params


    def general_symptoms_probabilitites(self):
        probabilities = self.get_probabilities()
        general_symptoms_params = {'Apathy':[],'Hypertension':[],'Cardiovascular':[],'DigitalAddiction':[],
                                   'DigitalConfusion':[]}
        if probabilities is not None:
            # Apathy Model
            self.p_apathy_stationaryBehaviour = parse_probabilities(probabilities, 'Apathy|stationaryBehaviour')
            self.p_apathy_dailyMotion = parse_probabilities(probabilities, 'Apathy|dailyMotion')
            self.p_apathy_leavingHouse = parse_probabilities(probabilities, 'Apathy|leavingHouse')
            self.p_apathy_digitalTimeUsage = parse_probabilities(probabilities, 'Apathy|digitalTimeUsage')
            self.p_apathy_overalMotion = parse_probabilities(probabilities, 'Apathy|overallMotion')
            self.p_apathy = parse_probabilities(probabilities, 'Apathy')

            # Hipertension Model
            self.p_hiperTen_bioMed = parse_probabilities(probabilities, 'Hipertension|biologicalMeasurements')
            self.p_hiperTen_medCond = parse_probabilities(probabilities, 'Hipertension|medicalCondition')
            self.p_hipertension = parse_probabilities(probabilities, 'Hipertension')

            # Cardiovascular condition Model
            self.p_cardiovascularCond_bioMes = parse_probabilities(probabilities,
                                                                   'Cardiovascular condition|biologicalMeasurements')
            self.p_cardiovascularCond_medCond = parse_probabilities(probabilities,
                                                                    'Cardiovascular condition|medicalCondition')
            self.p_cardiovascularCond = parse_probabilities(probabilities, 'Cardiovascular condition')

            # Digital Addiction Model
            self.p_digAdd_DTU = parse_probabilities(probabilities, 'digitalAddiction|digitalTimeUsage')
            self.p_digAdd_dep = parse_probabilities(probabilities, 'digitalAddiction|depression')
            self.p_digAdd = parse_probabilities(probabilities, 'digitalAddiction')

            # Digital Confusion Model
            self.p_digConf_ADB = parse_probabilities(probabilities, 'digitalConfusion|abnormalDigitalBehaviour')
            self.p_digConf = parse_probabilities(probabilities, 'digitalConfusion')


            general_symptoms_params['Apathy'] = [self.p_apathy, self.p_apathy_dailyMotion,
                                                 self.p_apathy_digitalTimeUsage, self.p_apathy_leavingHouse,
                                                 self.p_apathy_overalMotion, self.p_apathy_stationaryBehaviour]

            general_symptoms_params['Hypertension'] = [self.p_hipertension, self.p_hiperTen_bioMed,
                                                       self.p_hiperTen_medCond]
            general_symptoms_params['Cardiovascular'] = [self.p_cardiovascularCond, self.p_cardiovascularCond_bioMes,
                                                         self.p_cardiovascularCond_medCond]
            general_symptoms_params['DigitalAddiction'] = [self.p_digAdd, self.p_digAdd_dep, self.p_digAdd_DTU]
            general_symptoms_params['DigitalConfusion']= [self.p_digConf, self.p_digConf_ADB]

        return general_symptoms_params


    def alzheimer_motion_issues(self):
        probabilities = self.get_probabilities()
        alzheimer_motion_params = {'MovementIssue':[]}
        if probabilities is not None:
            # Movement Issues Model
            self.p_movIssue_fallDown = parse_probabilities(probabilities, 'MovementIssues|fallDown')
            self.p_movIssue_lossBalance = parse_probabilities(probabilities, 'MovementIssues|lossOfBalance')
            self.p_movIssue_statBeh = parse_probabilities(probabilities, 'MovementIssues|stationaryBehaviour')
            self.p_movIssue_dailyMotion = parse_probabilities(probabilities, 'MovementIssues|dailyMotion')
            self.p_movIssue_overallMotion = parse_probabilities(probabilities, 'MovementIssues|overallMotion')
            self.p_movIssue_leaveHouse = parse_probabilities(probabilities, 'MovementIssues|leavingHouse')
            self.p_movIssue = parse_probabilities(probabilities, 'MovementIssues')

            alzheimer_motion_params ['MovementIssue']=[self.p_movIssue, self.p_movIssue_dailyMotion,
                                                       self.p_movIssue_fallDown, self.p_movIssue_leaveHouse,
                                                       self.p_movIssue_lossBalance, self.p_movIssue_overallMotion,
                                                       self.p_movIssue_statBeh]
        return alzheimer_motion_params