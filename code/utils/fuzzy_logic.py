import matplotlib

#matplotlib.rcParams['backend'] = 'Qt5Agg'
#matplotlib.rcParams['backend.qt5'] = 'PySide2'

import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl


class FuzzyLogic():
    def __init__(self, data_name, data_value, hoenhyard, totalScore):

        # Fuzzy Logic Inputs Definition
        self.dataname = data_name
        self.data_value = data_value
        self.hoehnYard_value = hoenhyard
        self.totalScore_val = totalScore
        self.watch = False
        self.watch_output = False

        # Run the fuzzy logic algorithm
        self.run()

    def create_universe_variables(self):
        # Get the data to analyse
        if self.dataname == 'Postural':
            self.universe_variable = ctrl.Antecedent(np.arange(0, 5, 1), 'Postural')
        elif self.dataname == 'Balance':
            self.universe_variable = ctrl.Antecedent(np.arange(0, 10, 1), 'Balance')
        elif self.dataname == 'Walk':
            self.universe_variable = ctrl.Antecedent(np.arange(0, 25, 1), 'Walk')
        elif self.dataname == 'Involuntary Movements':
            self.universe_variable = ctrl.Antecedent(np.arange(0, 12, 1), 'Involuntary Movements')
        elif self.dataname == 'Postural Changes':
            self.universe_variable = ctrl.Antecedent(np.arange(0, 9, 1), 'Postural Changes')
        elif self.dataname == 'Movement Coordination':
            self.universe_variable = ctrl.Antecedent(np.arange(0, 6, 1), 'Movement Coordination')
        elif self.dataname == 'Rigidity':
            self.universe_variable = ctrl.Antecedent(np.arange(0, 56, 1), 'Rigidity')

        # Create the member function for the total score of the physiotherapy evaluation (only valid for 0-40)
        self.totalScore = ctrl.Antecedent(np.arange(0, 126, 1), 'Total Score')

        # Hoehn & Yard
        self.hoehnYard = ctrl.Antecedent(np.arange(0, 6, 1), 'Hoehn & Yard')

        # Output
        self.worry = ctrl.Consequent(np.arange(0, 11, 1), 'Worry Level')

    def create_membership_functions(self):
        levels = ['Mild', 'Mild-Moderate', 'Moderate', 'Moderate-Severe', 'Severe']

        # Evaluations
        self.universe_variable.automf(5, invert=False, names=levels)

        # Total Score
        # self.totalScore['Mild'] = fuzz.trimf(self.totalScore.universe, [15,15,40])
        # self.totalScore['Moderate']= fuzz.trimf(self.totalScore.universe, [35,40,40])

        self.totalScore['Mild'] = fuzz.zmf(self.totalScore.universe, 5, 25)
        self.totalScore['Moderate'] = fuzz.gaussmf(self.totalScore.universe, 35, 10)
        self.totalScore['Severe'] = fuzz.smf(self.totalScore.universe, 30, 100)

        # Hoenh & Yard
        self.hoehnYard['Estadio 1'] = fuzz.trimf(self.hoehnYard.universe, [1, 1, 2])
        self.hoehnYard['Estadio 2'] = fuzz.trimf(self.hoehnYard.universe, [1, 2, 3])
        self.hoehnYard['Estadio 3'] = fuzz.trimf(self.hoehnYard.universe, [2, 3, 4])
        self.hoehnYard['Estadio 4'] = fuzz.trimf(self.hoehnYard.universe, [3, 4, 5])
        self.hoehnYard['Estadio 5'] = fuzz.trimf(self.hoehnYard.universe, [4, 5, 5])

        # Final output
        self.worry['Low'] = fuzz.zmf(self.worry.universe, 0, 6)
        self.worry['Medium'] = fuzz.gaussmf(self.worry.universe, 4, 1)
        self.worry['High'] = fuzz.smf(self.worry.universe, 3, 10)

        # Watch the memberships functions
        if self.watch == True:
            self.universe_variable.view()
            self.totalScore.view()
            self.hoehnYard.view()
            self.worry.view()

    def create_rules(self):

        # Rules Definition
        # RULES FOR ESTADIO 1
        rule_1 = ctrl.Rule(self.universe_variable['Mild'] &
                           self.hoehnYard['Estadio 1'], self.worry['Medium'])

        rule_2 = ctrl.Rule(self.universe_variable['Mild-Moderate'] | self.universe_variable['Moderate'] |
                           self.universe_variable['Moderate-Severe'] | self.universe_variable['Severe'] &
                           self.hoehnYard['Estadio 1'], self.worry['High'])

        # RULES FOR ESTADIO 2
        rule_3 = ctrl.Rule(self.universe_variable['Mild'] | self.universe_variable['Mild-Moderate'] |
                           self.universe_variable['Moderate'] & self.hoehnYard['Estadio 2'] &
                           self.totalScore['Mild'], self.worry['Low'])

        rule_4 = ctrl.Rule(self.universe_variable['Mild'] | self.universe_variable['Mild-Moderate'] |
                           self.universe_variable['Moderate'] & self.hoehnYard['Estadio 2'] &
                           self.totalScore['Moderate'], self.worry['Medium'])

        rule_5 = ctrl.Rule(self.universe_variable['Moderate'] | self.universe_variable['Moderate-Severe'] &
                           self.hoehnYard['Estadio 2'] & self.totalScore['Mild'] |
                           self.totalScore['Moderate'], self.worry['Medium'])

        rule_6 = ctrl.Rule(self.universe_variable['Moderate'] | self.universe_variable['Moderate-Severe'] &
                           self.hoehnYard['Estadio 2'] & self.totalScore['Severe'],
                           self.worry['High'])

        rule_7 = ctrl.Rule(self.universe_variable['Severe'] &
                           self.hoehnYard['Estadio 2'] & self.totalScore['Mild'] |
                           self.totalScore['Moderate'] | self.totalScore['Severe'],
                           self.worry['High'])

        # RULES FOR ESTADIO 3-4
        rule_8 = ctrl.Rule(self.universe_variable['Mild'] | self.universe_variable['Mild-Moderate'] &
                           self.hoehnYard['Estadio 3'] | self.hoehnYard['Estadio 4'] & self.totalScore['Mild'] |
                           self.totalScore['Moderate'], self.worry['Low'])

        rule_9 = ctrl.Rule(self.universe_variable['Moderate'] | self.universe_variable['Moderate-Severe'] &
                           self.hoehnYard['Estadio 3'] | self.hoehnYard['Estadio 4'] &
                           self.totalScore['Mild'] | self.totalScore['Moderate'],
                           self.worry['Medium'])

        rule_10 = ctrl.Rule(self.universe_variable['Moderate'] | self.universe_variable['Moderate-Severe'] &
                           self.hoehnYard['Estadio 3'] | self.hoehnYard['Estadio 4'] &
                           self.totalScore['Severe'], self.worry['Medium'])

        rule_11 = ctrl.Rule(self.universe_variable['Severe'] & self.hoehnYard['Estadio 3'] |
                            self.hoehnYard['Estadio 4'] &
                            self.totalScore['Mild'] | self.totalScore['Moderate'],
                            self.worry['High'])

        # RULES FOR ESTADIO 5
        rule_12 = ctrl.Rule(self.universe_variable['Mild'] | self.universe_variable['Mild-Moderate'] |
                            self.universe_variable['Moderate'] &
                            self.hoehnYard['Estadio 5'] & self.totalScore['Mild'] |
                            self.totalScore['Moderate'], self.worry['Low'])

        rule_13 = ctrl.Rule(self.universe_variable['Mild'] | self.universe_variable['Mild-Moderate'] |
                            self.universe_variable['Moderate'] &
                            self.hoehnYard['Estadio 5'] & self.totalScore['Severe'],
                            self.worry['Medium'])

        rule_14 = ctrl.Rule(self.universe_variable['Moderate-Severe'] | self.universe_variable['Severe'] &
                            self.hoehnYard['Estadio 5'] & self.totalScore['Mild'] |
                            self.totalScore['Moderate'], self.worry['Medium'])

        rule_15 = ctrl.Rule(self.universe_variable['Moderate-Severe'] | self.universe_variable['Severe'] &
                            self.hoehnYard['Estadio 5'] & self.totalScore['Severe'],
                            self.worry['High'])

        self.rule_list = [rule_1, rule_2, rule_3, rule_4, rule_5, rule_6,
                          rule_7, rule_8, rule_9, rule_10, rule_11, rule_12,
                          rule_13, rule_14, rule_15]

    def create_control_system(self):
        self.mov_evolution_ctrl = ctrl.ControlSystem(self.rule_list)
        self.evaluate_movement = ctrl.ControlSystemSimulation(self.mov_evolution_ctrl)

    def compute_defuzzier(self):
        self.evaluate_movement.input[self.dataname] = self.data_value
        self.evaluate_movement.input['Hoehn & Yard'] = self.hoehnYard_value
        self.evaluate_movement.input['Total Score'] = self.totalScore_val
        self.evaluate_movement.compute()

    def show_output(self):
        # Print the level of worry
        if self.watch_output == True:
            self.worry.view(sim=self.evaluate_movement)
        print('Level of worry for this patient at', self.dataname, 'is:',
              self.evaluate_movement.output['Worry Level'])

    def run(self):
        try:
            self.create_universe_variables()
            self.create_membership_functions()
            self.create_rules()
            self.create_control_system()
            self.compute_defuzzier()
            self.show_output()
        except Exception as e:
            print(e)

    """def get_output(self):
        return self.evaluate_movement.output['Worry Level']"""