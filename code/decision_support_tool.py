# =============================================================================
# -------------------- DECISION SUPPORT TOOL VERSION 0.4 ----------------------
# =============================================================================
import os
import datetime
from modules.Notification import (RoutinesNotification,SymptomsNotification,
                                  TrainingNotification,MotionCognitiveNotification)
from modules.Routines import Routines
from modules.Symptoms import Symptoms
from modules.Correlation import Correlation
from modules.Predictions import Prediction
from modules.MotionCognitive import MotionCognitive
from modules.Training import Training
from utils.Services import Services
from utils.util import (parseJsonToDict,generate_dst_output,generate_data_visualization_files,
                        upload_file_cloud, downcload_file_cloud)

class DecisionSupportTool():
    def __init__(self):
        # Lambda function storage folders
        self.output_path = os.path.join('dst', 'out')
        self.input_path = os.path.join('ehr', 'out', 'dst')

        # Relative Data path
        self.input_tips_path = os.path.join('data', 'tips') #dst, code
        self.input_data_path = os.path.join('data', 'input')
        self.output_data_path = os.path.join('data', 'output')
        self.visualization_data_path = os.path.join('data', 'visualization')

        # Input output filenames
        self.filename_input = 'DST_INPUT_' + datetime.datetime.now().strftime("%Y%m%d") + '.json'
        self.filename_tips = 'tips.json'
        self.filename_output = 'DST_OUTPUT_' + datetime.datetime.now().strftime("%Y%m%d") + '.json'

        self.local_path_input = os.path.join(self.input_data_path, self.filename_input)
        self.local_path_output = os.path.join(self.output_data_path, self.filename_output)
        self.local_path_tips = os.path.join(self.input_tips_path, self.filename_tips)

        # Download Last File
        success = downcload_file_cloud(self.output_path, self.input_path, self.local_path_input)
        if success==0:
            self.filename_input = 'EHR_DATA_20190122.json'
            self.local_path_input = os.path.join(self.input_data_path, self.filename_input)
            #self.data_dst = parseJsonToDict(self.local_path_input)
            self.filename_output= self.filename_input.replace('EHR_DATA', 'DST_OUTPUT')
            self.local_path_output = os.path.join(self.output_data_path, self.filename_output)

        # Read Data
        self.tipsFile = parseJsonToDict(self.local_path_tips)[0]
        self.data_dst = parseJsonToDict(self.local_path_input)

    # ==================================================================================================================
    def run(self):
        end_ok = False
        print(100*'=')
        print("Running Decision Support Tool!")
        print(100 * '=')
        # Run DST Module
        for data in self.data_dst:
            if data:
                try:
                    # Set Up environment
                    error = self.set_up_dst(data)

                    if not error:
                        # 1) Launch routines
                        self.launch_routines()

                        # 2) Launch Symptoms
                        self.launch_symptoms()

                        # 3) Launch Training
                        training_not = self.launch_training()
                        # 4) Launch cognitive
                        self.launch_motion_cognitive()

                        # Extract Training Documents
                        if training_not is not None:
                            training_docs = training_not.data['Documents']
                        else:
                            training_docs = {}

                        # 5) Correlations Module
                        corr_output = self.launch_correlation_module()

                        # 6) Predictions Module
                        pred_output = self.launch_prediction_module()

                        # 7) Generate Output
                        self.prepare_dst_output(pred_output, corr_output, training_docs,filename=self.local_path_output,
                                                    uuid=self.user)

                        # 8) Generate Visualization Data Output
                        self.prepare_visualization_data(prediction_data=pred_output, correlation_data=corr_output)
                        end_ok = True
                except Exception as e:
                    print(e)
        return end_ok

    def set_up_dst(self, input_data):
        print('Setting up the environment ...')
        error = False
        try:
            # Initialise Connection to the Services
            self.catalog = Services(input_data)
            # Read the recommendations/tips file

            # Get data from DST input and from the Services
            self.patientName, self.patientID, self.language, self.disease, self.disease_phase,\
            self.comorbidities = self.catalog.get_patient_information()
            self.user = self.patientID
            self.date = self.catalog.get_date()
            #self.output_filename = 'DST_' + datetime.datetime.now().strftime("%Y%m%d") + '.json'
            self.professionals = self.catalog.get_professionals()
            self.scales = self.catalog.get_scales()
            self.medication = self.catalog.get_medical_information()
            self.probabilities = self.catalog.get_probabilities()
            if self.disease=='Parkinson':
                self.movement_evolution = self.catalog.get_movement_evolution_information()
            else:
                self.movement_evolution = None
        except Exception as e:
            print(e)
            print('Unable to extract the data required for DST. Please review the input file')
            error = True
        return error
 
    # ==================================================================================================================
    def launch_routines(self):
        """ Launches the routines module of the Decision Support tool."""
        print(100*'=')
        print("1) Launching Routines Module!")
        print(100*'=')
        try:
            self.routines_data = {'Name':self.patientName, 'ID':self.patientID,
                                  'Lang':self.language,'Disease':self.disease,
                                  'Disease Phase':self.disease_phase,
                                  'Comorbidities':self.comorbidities,
                                  'Professionals':self.professionals,
                                  'Medication':self.medication,
                                  'TipsFile':self.tipsFile,
                                  'Probabilities':self.probabilities,
                                  'Insomnia':self.catalog.get_information_by_name('insomnia'),
                                  'NightMotion':self.catalog.get_information_by_name('nightMotion'),
                                  'Scales':self.scales,
                                  'VisitsBathroom': self.catalog.get_information_by_name('visitBathroom'),
                                  'Incontinence':self.catalog.get_information_by_name('incontinence'),
                                  'Catalog':self.catalog}
            # Launch the module!
            routine_module = Routines(self.routines_data)
            routines_output = routine_module.generate_output()

            # Launch Recommendation
            patient_information = {'Patient Name': self.patientName, 'Patient ID':self.patientID,
                                   'Disease': self.disease, 'Disease Phase': self.disease_phase}
            routine_not = RoutinesNotification(patient_information=patient_information, professionals=self.professionals,
                                               data=routines_output, language=self.language, tipsFile=self.tipsFile)
            routine_not.launch_notification()
        except Exception as e:
            print(e)
            print('Unable to analyse properly the Routines Module.')
    # ==================================================================================================================
    def launch_symptoms(self):
        print(100*'=')
        print("2) Launching Symptoms Module!")
        print(100*'=')
        try:
            self.moduleName = 'Symptoms'
            # Getting data required to symptoms

            self.general_symptoms_data = self.catalog.general_symptoms_probabilitites()

            if self.disease=='Parkinson':
                self.disease_symptoms_data = self.catalog.parkinson_symptoms_probabilities()
            elif self.disease=='Alzheimer':
                self.disease_symptoms_data = self.catalog.alzheimer_symptoms_probabilities()

            self.symptoms_data = {'Name':self.patientName, 'ID':self.patientID,
                                  'Lang':self.language,'Disease':self.disease,
                                  'Disease Phase':self.disease_phase,
                                  'Comorbidities':self.comorbidities,
                                  'Professionals':self.professionals,
                                  'Medication':self.medication,
                                  'TipsFile':self.tipsFile,
                                  'General symptoms': self.general_symptoms_data,
                                  'Disease symptoms':self.disease_symptoms_data,
                                  'Date':self.date,
                                  'Catalog':self.catalog}
            # Launch the module
            symptoms_module = Symptoms(self.symptoms_data)
            output_symptoms = symptoms_module.generate_output()

            # Launch Recommendation
            patient_information = {'Patient Name': self.patientName, 'Patient ID': self.patientID,
                                   'Disease': self.disease, 'Disease Phase': self.disease_phase}
            symptoms_not = SymptomsNotification(patient_information=patient_information, professionals=self.professionals,
                                               data=output_symptoms, language=self.language, tipsFile=self.tipsFile)
            symptoms_not.launch_notification()
        except Exception as e:
            print(e)
            print('Unable to analyse properly the Symptoms Module.')

    # ==================================================================================================================
    def launch_correlation_module(self):
        print(100*'=')
        print("5) Launching Correlation module!")
        print(100*'=')
        corr_output = {'Correlations':{}}
        try:
            self.correlation_params = {'Name':self.patientName, 'ID':self.patientID,
                                       'Lang':self.language, 'Disease':self.disease,
                                       'Disease Phase':self.disease_phase,
                                       'Comorbidities':self.comorbidities,
                                       'Medication':self.medication,
                                       'Probabilities':self.probabilities,
                                       'Scales':self.scales,
                                       'Catalog':self.catalog,
                                       'Dates':self.date,
                                       'TipsFile':self.tipsFile}
            self.corr = Correlation(self.correlation_params)
            corr_output = self.corr.generate_output()
        except Exception as e:
            print(e)
            print('Unable to analyse properly the Correlation Module.')
        return corr_output
    # ==================================================================================================================
    def launch_prediction_module(self):
        print(100*'=')
        print("6) Launching Prediction module!")
        print(100*'=')
        pred_output = {'Predictions':{}}
        try:
            self.prediction_params = {'Name':self.patientName, 'ID':self.patientID,
                                       'Lang':self.language, 'Disease':self.disease,
                                       'Disease Phase':self.disease_phase,
                                       'Comorbidities':self.comorbidities,
                                       'Medication':self.medication,
                                       'Probabilities':self.probabilities,
                                       'Scales':self.scales,
                                       'Dates':self.date,
                                       'Movement evolution': self.movement_evolution,
                                       'Catalog':self.catalog}
            self.pred = Prediction(self.prediction_params)
            pred_output = self.pred.generate_output()
        except Exception as e:
            print(e)
            print('Unable to analyse properly the Prediction Module.')
        return pred_output
    # ==================================================================================================================
    def launch_motion_cognitive(self):
        print(100*'=')
        print("4) Launching Motion % Cognitive module!")
        print(100*'=')
        try:
            self.motionCog_params = {'Name':self.patientName, 'ID':self.patientID,
                                     'Lang':self.language, 'Disease':self.disease,
                                     'Disease Phase':self.disease_phase,
                                     'Comorbidities':self.comorbidities,
                                     'Medication':self.medication,
                                     'Probabilities':self.probabilities,
                                     'Scales':self.scales,
                                     'Dates':self.date,
                                     'Movement evolution': self.movement_evolution,
                                     'Catalog':self.catalog}
            self.motCog = MotionCognitive(self.motionCog_params)
            motCog_output = self.motCog.generate_output()

            patient_information = {'Patient Name': self.patientName, 'Patient ID': self.patientID,
                                   'Disease': self.disease, 'Disease Phase': self.disease_phase}
            motCog_not = MotionCognitiveNotification(patient_information=patient_information, professionals=self.professionals,
                                                data=motCog_output, language=self.language, tipsFile=self.tipsFile)
            motCog_not.launch_notification()
        except Exception as e:
            print(e)
            print('Unable to analyse properly the Motion & Cognitive Module.')
    # ==================================================================================================================
    def launch_training(self):
        print(100*'=')
        print("3) Launching Training module!")
        print(100*'=')
        training_not = None
        try:
            self.training_params = {'Name':self.patientName, 'ID':self.patientID,
                                    'Lang':self.language, 'Disease':self.disease,
                                    'Disease Phase':self.disease_phase,
                                    'Comorbidities':self.comorbidities,
                                    'Scales':self.scales,
                                    'Catalog':self.catalog,}
            self.training = Training(self.training_params)
            training_output = self.training.generate_output()
            patient_information = {'Patient Name': self.patientName, 'Patient ID': self.patientID,
                                   'Disease': self.disease, 'Disease Phase': self.disease_phase}
            training_not = TrainingNotification(patient_information=patient_information, professionals=self.professionals,
                                               data=training_output, language=self.language, tipsFile=self.tipsFile)
            training_not.launch_notification()
        except Exception as e:
            print(e)
            print('Unable to analyse properly the Training Module.')

        return training_not

    # ==================================================================================================================
    def prepare_dst_output(self, prediction_data, correlation_data, recommendations,
                           filename=None, uuid=None):
        print(100*'=')
        print("Generating DST output ...")
        print(100*'=')
        try:
            output = []
            output.append(correlation_data)
            output.append(prediction_data)

            if recommendations is not None:
                output.append({'Training Documents': recommendations})
            else:
                output.append({'Training Documents':[]})
            # Save local copy
            generate_dst_output(output_filename=filename, content=output, user=uuid)

            # Upload File to the cloud
            upload_file_cloud(self.output_path, self.input_path, self.output_data_path, self.filename_output)

        except Exception as e:
            print(e)
            print('Unable to generate the DST output')

    # ==================================================================================================================
    def prepare_visualization_data(self, prediction_data, correlation_data):
        print(100*'=')
        print('Generating Visualization Data File')
        print(100*'=')
        ok = False
        try:
            keys = ['overallMotion_prediction', 'heartRate_prediction',
                    'nightMotion_prediction', 'visitBathroom']
            uuid = self.patientID
            if self.disease=='Parkinson':
                keys.append('fallDown_prediction')
                keys.append('lossBalance_prediction')
                keys.append('freezing_prediction')
                keys.append('festination_prediction')
            elif self.disease=='Alzheimer':
                keys.append('leavingHouse_prediction')
                keys.append('abnormalDigitalBehaviours_prediction')

            data = [prediction_data]
            # 1) Event Chart
            generate_data_visualization_files(label='chart', uuid=uuid, dates=self.date, data=data,
                                              output_path=self.output_path, input_path=self.input_path,
                                              output_data_path=self.visualization_data_path)
            # 2) Event Correlation
            data_cor = []
            #data_cor = correlation_data
            data_cor.append(correlation_data)
            generate_data_visualization_files(label='correlation', uuid=uuid, dates=self.date,
                                              data=data_cor, output_path=self.output_path,
                                              input_path=self.input_path,
                                              output_data_path=self.visualization_data_path)
            ok= True
        except Exception as e:
            print(e)
            print('Unable to generate the Data visualization file')

        return ok