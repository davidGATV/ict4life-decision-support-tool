# Decision Support Tool

## 1) Introduction

The development of the Decision Support Tool (DST)  is a part of the European Project ICT4Life where a whole intelligent system is implemented in order to help both health professionals and patiens and caregivers providing them with additional information and understanding about the evolution of the disorder.

The main objetive of this module is to analyse different aspects of the patient in order to find possible abnormal behaviour and provide to both caregivers and patients with information and recommendations for the improvement of the quality of life of all of them.

The DST contains 4 different modules including: routines, symptoms, training and motion&cognitive. Each module analyses different aspects of both Parkinson's and Alzheimer's diseases. Routines and training are common for both disorders whereas as symptoms and motion&cognitive are speficic for each disorder.


![Image](/Images/routines_v2.png)

![Image](/Images/symptoms.png)

![Image](/Images/symptoms_v2.png)

## 2) Software Architecture

The DST model contains different sub-modules including: Routines, Symptoms, Training and Motion & Cognitive. Each module is based on tree Based Model and the Motion & Cognitive includes a fuzzy logic algorithm for predicting the state of a patient based on its physiotherapy evaluation. All the moduls launch different notifications depending on the decision made to both patients, caregivers and health professionals as well.

														init_dst.py
														  |
														  |
												DecisionSupportTool.py
														  |
														  | 
								 --------------------------------------------------------
								|					|				|					|
								|					|				| 					|
							Routines.py        	Symptoms.py 	Training.py  	Motion & Cognitive.py


## 2) Run 
To run the DST module, the following script should be call:

- Go to code folder
- Execute init_dst.py

init_dst.py makes a call to the DecisionSupportTool class which has all the implementation of the module. Once the module has been called, all the sub-modules are launched and the corresponding recommendations and tips are sending in case of the situation is detected as abnormal.
Finally, an output file with json format is created and updated to the corresponding cloud folder for the DST.

