FROM python:3.5

RUN mkdir  /dst

ARG ACCESS_KEY
ARG SECRET_KEY

ENV AWS_ACCESS_KEY_ID=$ACCESS_KEY
ENV AWS_SECRET_ACCESS_KEY=$SECRET_KEY

COPY . /dst

RUN pip install -r /dst/requirements.txt
RUN pip install -r /dst/requirements_sk.txt